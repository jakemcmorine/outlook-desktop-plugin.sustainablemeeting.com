<?php

class Invite
{
    private $conn;
    private $table_name = "mfc_meeting_invites";

    public $Id;
    public $ClientId;
    public $OrganiserId;
    public $FirstName;
    public $LastName;
    public $Email;
    public $Title;
    public $Company;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function create(){

        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    client_id = :client_id,
                    organiser_id = :organiser_id,
                    first_name = :first_name,
                    last_name = :last_name,
                    email_address = :email_address,
                    title = :title,
                    company = :company";

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->ClientId=htmlspecialchars(strip_tags($this->ClientId));
        $this->FirstName=htmlspecialchars(strip_tags($this->FirstName));
        $this->LastName=htmlspecialchars(strip_tags($this->LastName));
        $this->Email=htmlspecialchars(strip_tags($this->Email));
        $this->Title=htmlspecialchars(strip_tags($this->Title));
        $this->OrganiserId=htmlspecialchars(strip_tags($this->OrganiserId));
        $this->Company=htmlspecialchars(strip_tags($this->Company));


        // bind the values
        $stmt->bindParam(':client_id', $this->ClientId);
        $stmt->bindParam(':organiser_id', $this->OrganiserId);
        $stmt->bindParam(':first_name', $this->FirstName);
        $stmt->bindParam(':last_name', $this->LastName);
        $stmt->bindParam(':email_address', $this->Email);
        $stmt->bindParam(':title', $this->Title);
        $stmt->bindParam(':company', $this->Company);

        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }

        return false;
    }

}

?>