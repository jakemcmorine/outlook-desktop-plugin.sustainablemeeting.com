<?php
class Organiser
{
    private $conn;
    private $table_name = "mfc_meeting_organiser";

    public $Id;
    public $ClientId;
    public $MeetingInfoId;
    public $FirstName;
    public $LastName;
    public $Email;
    public $Title;
    public $Company;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function create(){

        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    client_id = :client_id,
                    meeting_info_id = :meeting_info_id,
                    first_name = :first_name,
                    last_name = :last_name,
                    email_address = :email_address,
                    title = :title,
                    company = :company";

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->ClientId=htmlspecialchars(strip_tags($this->ClientId));
        $this->FirstName=htmlspecialchars(strip_tags($this->FirstName));
        $this->LastName=htmlspecialchars(strip_tags($this->LastName));
        $this->Email=htmlspecialchars(strip_tags($this->Email));
        $this->Title=htmlspecialchars(strip_tags($this->Title));
        $this->MeetingInfoId=htmlspecialchars(strip_tags($this->MeetingInfoId));
        $this->Company=htmlspecialchars(strip_tags($this->Company));


        // bind the values
        $stmt->bindParam(':client_id', $this->ClientId);
        $stmt->bindParam(':meeting_info_id', $this->MeetingInfoId);
        $stmt->bindParam(':first_name', $this->FirstName);
        $stmt->bindParam(':last_name', $this->LastName);
        $stmt->bindParam(':email_address', $this->Email);
        $stmt->bindParam(':title', $this->Title);
        $stmt->bindParam(':company', $this->Company);


        if($stmt->execute()){
            $LastId  = $this->conn->lastInsertId();
            return $LastId;
        }

        return -1;
    }

}

?>