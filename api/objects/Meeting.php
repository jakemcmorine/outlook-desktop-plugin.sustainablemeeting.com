<?php

class Meeting
{

    // database connection and table name
    private $conn;
    private $table_name = "mfc_meeting_info";

    // object properties
    public $Id;
    public $ClientId;
    public $MeetingId;
    public $Subject;
    public $Location;
    public $Type;
    public $StartDate;
    public $EndDate;
    public $CreatedAt;
    public $Cancelled;
    public $State;
    public $CancelDate;

    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

    function read()
    {
        // select all query
        $query = "SELECT *  FROM " . $this->table_name . " ORDER BY Subject";
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    function Cancel()
    {
        $query = "UPDATE " . $this->table_name . " SET cancelled = '1' ,cancellation_date = CURRENT_TIMESTAMP() WHERE id = :id ";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id', $this->id);
        echo $query;

         if($stmt->execute()) {
             return true;
         }
          return false;
    }



    function create(){

        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    client_id = :client_id,
                    meeting_id = :meeting_id,
                    subject = :subject,
                    location = :location,
                    type = :type,
                    start_date = :start_date,
                    end_date = :end_date";

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->ClientId=htmlspecialchars(strip_tags($this->ClientId));
        $this->MeetingId=htmlspecialchars(strip_tags($this->MeetingId));
        $this->Subject =htmlspecialchars(strip_tags($this->Subject));
        $this->Location=htmlspecialchars(strip_tags($this->Location));
        $this->Type=htmlspecialchars(strip_tags($this->Type));
        $this->StartDate=htmlspecialchars(strip_tags($this->StartDate));
        $this->EndDate=htmlspecialchars(strip_tags($this->EndDate));

        $stmt->bindParam(':client_id', $this->ClientId);
        $stmt->bindParam(':meeting_id', $this->MeetingId);
        $stmt->bindParam(':subject', $this->Subject);
        $stmt->bindParam(':location', $this->Location);
        $stmt->bindParam(':type', $this->Type);
        $stmt->bindParam(':start_date', $this->StartDate);
        $stmt->bindParam(':end_date', $this->EndDate);

        if($stmt->execute()){
            $LastId  = $this->conn->lastInsertId();
            return $LastId;
        }

        return -1;
    }

}

?>
