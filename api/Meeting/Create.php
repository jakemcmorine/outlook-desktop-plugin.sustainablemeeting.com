<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/core.php';
include_once '../jwt/BeforeValidException.php';
include_once '../jwt/ExpiredException.php';
include_once '../jwt/SignatureInvalidException.php';
include_once '../jwt/JWT.php';

use \Firebase\JWT\JWT;

include_once '../config/database.php';
include_once '../objects/Meeting.php';
include_once '../objects/Organiser.php';
include_once '../objects/Invite.php';


// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate product object
$Meeting = new Meeting($db);
$Organiser = new Organiser($db);

$data = json_decode(file_get_contents("php://input"));

$jwt = isset($data->jwt) ? $data->jwt : "";

if ($jwt) {
    try {
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        $Meeting->Subject = $data->subject;
        $Meeting->Location = $data->location;
        $Meeting->Type = $data->type;
        $Meeting->StartDate = $data->start;
        $Meeting->EndDate = $data->end;
        $Meeting->MeetingId = $data->OutlookID;
        $Meeting->ClientId = $decoded->data->id;

        $MeetingId = $Meeting->create();
        if ($MeetingId != -1) {
            $Meeting->Id = $MeetingId;


            $Organiser->ClientId = $decoded->data->id;
            $Organiser->MeetingInfoId = $Meeting->Id;
            $Organiser->FirstName = $data->organiser->firstname;
            $Organiser->LastName = $data->organiser->lastname;
            $Organiser->Email = $data->organiser->email;
            $Organiser->Title = $data->organiser->title;
            $Organiser->Company = $data->organiser->company;

            $OrganiserId = $Organiser->create();

            if ($OrganiserId != -1) {
                $Organiser->Id = $MeetingId;


                if (!empty($data->invites)) {
                    foreach($data->invites as $invite) {

                        $Invite = new Invite($db);

                        $Invite->Company = $invite -> company;
                        $Invite->FirstName = $invite -> firstname;
                        $Invite->LastName = $invite -> lastname;
                        $Invite->Email = $invite -> email;
                        $Invite->Title = $invite -> title;
                        $Invite->OrganiserId = $Organiser->Id;
                        $Invite->ClientId = $decoded->data->id;

                        $Invite->create();
                    }
                }

                http_response_code(200);

                echo json_encode(
                    array(
                        "message" => "Meeting was Created."
                    )
                );
            } else {
                http_response_code(401);
                echo json_encode(array("message" => "Error while add Organiser"));
            }
        } else {
            http_response_code(401);
            echo json_encode(array("message" => "Error while add meeting"));
        }


    } catch (Exception $e) {
        http_response_code(401);

        echo json_encode(array(
            "message" => "Access denied.",
            "error" => $e->getMessage()
        ));
    }
} else {

    http_response_code(401);
    echo json_encode(array("message" => "Access denied."));
}

?>