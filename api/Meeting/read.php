<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");


include_once ('..\config\database.php');
include_once('../objects/Meeting.php');

$database = new Database();
$db = $database->getConnection();

// initialize object
$meeting = new Meeting($db);


// query products
$stmt = $meeting->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){

  // products array
  $meetings_arr=array();
  $meetings_arr["data"]=array();

  // retrieve our table contents
  // fetch() is faster than fetchAll()
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    // extract row
    // this will make $row['name'] to
    // just $name only
    extract($row);

    $meeting_item=array(
      "id"  =>                   $id,
      "ClientId" =>              $client_id,
      "MeetingId" =>             $meeting_id,
      "subject" =>               $subject,
      "Location" =>              $location,
      "Type" =>                  $type,
      "start_date" =>            $start_date,
      "end_date" =>              $end_date,
      "created_at" =>            $created_at,
      "cancelled" =>             $cancelled,
      "state" =>                 $state,
      "cancellation_date" =>     $cancellation_date
    );



    array_push($meetings_arr["data"], $meeting_item);
  }

  // set response code - 200 OK
  http_response_code(200);

  // show products data in json format
  echo json_encode($meetings_arr);
}
else{

  // set response code - 404 Not found
  http_response_code(404);

  // tell the user no products found
  echo json_encode(
    array("message" => "No products found.")
  );
}


?>
