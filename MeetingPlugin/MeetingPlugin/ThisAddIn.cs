﻿using MeetingPlugin.Code.API.Controller;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Controller;
using MeetingPlugin.Code.Forms;
using MeetingPlugin.Code.Helper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;
using Timer = System.Threading.Timer;

namespace MeetingPlugin
{
    public partial class ThisAddIn
    {
        private static string VersionInfo = "1.1.20";
        
        //static Timer TTimer{ get; set; }
        private static Timer UpdateTimer { get; set; }

        public static Timer CalendarUpdateTimer { get; set; }

        public static bool PluginStart = true;
        public static List<MeetingEntity> meetingObjectsList = new List<MeetingEntity>();
        public static List<string> ResponseStatusList = new List<string>();
        public OutlookMonitor Monitor  { get; set; }

        Outlook.Inspectors inspectors;

        public static Outlook.Inspector theCurrentAppointment;
        public UserEntity UserInfo { get; set; }
        static List<string> UserEmails { get; set; }
        private async void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            try
            {
                Log.Info($"ThisAddIn_Startup Started....");
                var token = await Option.GetToken();
                Option.IsActive = !string.IsNullOrEmpty(token);
                if (Option.IsActive)
                {
                    string outlook = $"{Application.Name} {Application.Version}", build = "", v = "";
                    var process = Process.GetCurrentProcess();

                    if (process.MainModule != null)
                    {
                        FileVersionInfo majorVersion = FileVersionInfo.GetVersionInfo(process.MainModule.FileName);
                        build = majorVersion.ProductVersion;
                        v = $"{majorVersion.ProductBuildPart}.{majorVersion.ProductPrivatePart}";
                        outlook = $"{majorVersion.ProductName} {Application.Version}";

                        string versionRegKey = $"{majorVersion.ProductMajorPart}.{majorVersion.ProductMinorPart}";
                        RegistryUpdate("Sustainably Run Meetings", 1, versionRegKey);
                    }


                    VersionController version = new VersionController();
                    Option.IsActive = await version.IsEnable(new VersionEntity()
                    {
                        Outlook = outlook,
                        Version = v,
                        Build = build
                    });

                    if (!Option.IsActive)
                        return;

                    //IsWebPluginEnabled = CheckWebPluginEnabled();
                    //await CheckWebPluginEnabled();
                    if (UserInfo == null)
                    {
                        Log.Info($"ThisAddin - UserInfo is null. Wait for 0.5 sec...");
                        Thread.Sleep(500);
                    }
                    if (UserInfo != null && UserInfo.webPluginEnabled) // If Webplugin is active, no need to Enable this Desktop plugin.
                    {
                        Log.Info($"ThisAddin - Web Plugin Enabled....");
                        Option.IsActive = false;
                        return;
                    }
                    else
                    {
                        Log.Info($"ThisAddin - Web Plugin Disabled....");
                        inspectors = this.Application.Inspectors;
                        inspectors.NewInspector += new Outlook.InspectorsEvents_NewInspectorEventHandler(Inspectors_NewInspector);
                        //AppController.Queue = new List<AppEntity>();
                        //TTimer = new Timer(new TimerCallback(TickTimer), null, 10000, 10000);
                        UpdateTimer = new Timer(new TimerCallback(UpdateTick), null, new TimeSpan(0, 0, 0), new TimeSpan(1, 0, 0));
                        Monitor = new OutlookMonitor();
                        await Monitor.Initialize();
                        CalendarUpdateTimer = new Timer(new TimerCallback(Monitor.CalendarUpdateTimer), null, new TimeSpan(0, 0, 0), new TimeSpan(0, 0, 10));
                    }
                    Log.Info($"ThisAddIn_Startup Completed....");

                }
                else
                {
                    Log.Info("Invalid Token");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ThisAddIn_Startup");
            }

        }

        public async Task CheckWebPluginEnabled()
        {
            //bool IsWebPluginEnabled = false;
            UserEmails = new List<string>();
            Log.Info("CheckWebPluginEnabled Starting..");
            //foreach (Outlook.Account account in Application.Session.Accounts)
            //{
            try
            {
                string email = "";
                Outlook.AddressEntry accountEmail = Application.Session.CurrentUser.AddressEntry;
                if (accountEmail != null)
                {
                    Log.Info("CheckWebPluginEnabled accountEmail : " + accountEmail.AddressEntryUserType);
                    if (accountEmail.AddressEntryUserType ==
                            Outlook.OlAddressEntryUserType.olExchangeUserAddressEntry ||
                            accountEmail.AddressEntryUserType ==
                            Outlook.OlAddressEntryUserType.olExchangeRemoteUserAddressEntry)
                    {
                        Log.Info("CheckWebPluginEnabled accountEmail GetExchangeUser");
                        Outlook.ExchangeUser exchUser = accountEmail.GetExchangeUser();
                        if (exchUser != null)
                        {
                            email = exchUser.PrimarySmtpAddress;
                        }
                        Log.Info("CheckWebPluginEnabled accountEmail GetExchangeUser Over." + email);   
                    }
                    else
                    {
                        Log.Info("CheckWebPluginEnabled accountEmail GetSmtpAddress");
                        //email = account.SmtpAddress;
                        email = Application.Session.CurrentUser.GetSmtpAddress();
                        Log.Info("CheckWebPluginEnabled accountEmail GetSmtpAddress Over: " + email);
                        if (string.IsNullOrEmpty(email))
                        {
                            email = accountEmail.Address;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(email))
                {
                    //IsWebPluginEnabled = true;
                    Log.Info($"Checking web plugin enabled for ({email})");
                    UserEmails.Add(email);
                    UserInfo = await Option.CheckWebPluginForUser(UserEmails);
                    Log.Info($"Checking web plugin enabled ({UserInfo.webPluginEnabled})");
                }
                else
                {
                    Log.Info("CheckWebPluginEnabled - Cannot check because Email cannot be fetched");
                }

                Log.Info("CheckWebPluginEnabled Completed..");
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "error while CheckWebPluginEnabled.");
            }
            //}

        }

        private void Inspectors_NewInspector(Outlook.Inspector Inspector)
        {
            theCurrentAppointment = Inspector;
        }

        private void RegistryUpdate(string KeyName, object Value, string versionRegKey)
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser;
                string subKey1 = @"Software\Microsoft\Office\";
                string subKey2 = @"\Outlook\Resiliency\";

                string disableListKey = "DoNotDisableAddinList";

                string disableListSubKey = $"{subKey1}{versionRegKey}{subKey2}{disableListKey}";
                
                //using (RegistryKey disableRegKey = rk.CreateSubKey(disableListSubKey))
                //{
                //    disableRegKey.SetValue(KeyName, Value);
                //}

                #region NotificationReminderSubKeyUpdation
                string notificationReminderKey = "NotificationReminderAddinData";
                string notificationReminderSubKey = $"{subKey1}{versionRegKey}{subKey2}{notificationReminderKey}";

                using (RegistryKey disableRegKey = rk.CreateSubKey(disableListSubKey),
                    notifReminderKey = rk.CreateSubKey(notificationReminderSubKey))
                {
                    disableRegKey.SetValue(KeyName, Value);
                    notifReminderKey.SetValue("Sustainably Run Meetings\\dtype", 0);
                    notifReminderKey.SetValue("Sustainably Run Meetings", Int32.MaxValue, RegistryValueKind.DWord);
                }
                #endregion
            }
            catch (Exception e)
            {
                Log.Error(e, "Writing registry " + KeyName);
            }
        }
        /*
        static void TickTimer(object state)
        {
            TTimer.Change(Timeout.Infinite, Timeout.Infinite);

            try
            {
                if (AppController.Queue.Count > 0)
                {
                    bool result = false;
                    var _CalendarController = new CalendarController();
                    var newItems = AppController.Queue.Where(c => c.IsDone == false);
                    if (newItems.Any())
                    {
                        foreach (var entity in newItems)
                        {
                            var item = Globals.ThisAddIn.Application.Session.GetItemFromID(entity.MeetingId);

                            Outlook.MeetingItem meeting = item as Outlook.MeetingItem;
                            Outlook.AppointmentItem appointment = item as Outlook.AppointmentItem;

                            if (entity.Operation == OperationEntity.Add)
                            {
                                if (item is Outlook.MeetingItem)
                                {
                                    appointment = meeting.GetAssociatedAppointment(false);
                                    result = _CalendarController.Add(meeting).Result;
                                }

                                if (item is Outlook.AppointmentItem)
                                {
                                    result = _CalendarController.Add(appointment).Result;
                                }
                                entity.IsDone = true;
                            }
                            else if (entity.Operation == OperationEntity.Edit)
                            {
                                if (item is Outlook.MeetingItem)
                                {
                                    appointment = meeting.GetAssociatedAppointment(false);
                                    result = _CalendarController.Edit(meeting).Result;
                                }

                                if (item is Outlook.AppointmentItem)
                                {
                                    result = _CalendarController.Edit(appointment).Result;
                                }
                                entity.IsDone = true;
                            }
                            else if (entity.Operation == OperationEntity.Cancel)
                            {
                                if (item is Outlook.MeetingItem)
                                {
                                    appointment = meeting.GetAssociatedAppointment(false);
                                    result = _CalendarController.Cancel(meeting).Result;
                                }

                                if (item is Outlook.AppointmentItem)
                                {
                                    result = _CalendarController.Cancel(appointment).Result;
                                }

                                entity.IsDone = true;
                            }
                            else if (entity.Operation == OperationEntity.Status)
                            {
                                if (item is Outlook.MeetingItem)
                                {
                                    appointment = meeting.GetAssociatedAppointment(false);
                                    result = _CalendarController.Status(meeting, entity.Email).Result;
                                }

                                if (item is Outlook.AppointmentItem)
                                {
                                    result = _CalendarController.Status(appointment, entity.Email).Result;
                                }
                                entity.IsDone = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "TickTimer");
            }
            finally
            {
                TTimer.Change(10000, 10000);
            }
        }
        //*/
        static async void UpdateTick(object state)
        {
            //Stop Timer
            UpdateTimer.Change(Timeout.Infinite, Timeout.Infinite);
            try
            {
                string db = await VersionController.GetVersion(UserEmails, VersionInfo);
                if (Version.TryParse(db, out var dbVersion))
                {
                    Version.TryParse(VersionInfo, out var currentVersion);

                    if (dbVersion > currentVersion)
                    {
                        FormUpdate frm = new FormUpdate(dbVersion, currentVersion);
                        frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "UpdateTick");
            }
            finally
            {
                UpdateTimer.Change(new TimeSpan(1, 0, 0), new TimeSpan(1, 0, 0));
            }
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see https://go.microsoft.com/fwlink/?LinkId=506785
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
