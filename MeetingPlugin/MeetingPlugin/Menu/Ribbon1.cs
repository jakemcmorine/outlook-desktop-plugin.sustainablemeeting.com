﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using MeetingPlugin.Code.Controller;
using Microsoft.Office.Tools.Ribbon;

namespace MeetingPlugin
{
    public partial class Ribbon1
    {
        private async void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {
            try
            {
                Log.Info($"Ribbon1_Load Started....");
                BtnInfo.Visible = false;
                BtnInfo1.Visible = false;
                BtnInfo2.Visible = false;
                await Globals.ThisAddIn.CheckWebPluginEnabled();

                if (Globals.ThisAddIn.UserInfo != null && Globals.ThisAddIn.UserInfo.webPluginEnabled) // If Webplugin is active, no need to Enable this Desktop plugin.
                {
                    Log.Info($"Ribbon1_Load Web Plugin Enabled....");
                    //Globals.ThisAddIn.Option.IsActive = false;
                }
                else
                {
                    Log.Info($"Ribbon1_Load Web Plugin Disabled....");
                    BtnInfo.Visible = true;
                    BtnInfo1.Visible = true;
                    BtnInfo2.Visible = true;

                }
                Log.Info($"Ribbon1_Load Completed....");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Ribbon1_Load Error ");
            }

        }

        private void BtnInfo_Click(object sender, RibbonControlEventArgs e)
        {                       
            Process.Start(Globals.ThisAddIn.Monitor.UserInfo.Url);
        }
    }
}
