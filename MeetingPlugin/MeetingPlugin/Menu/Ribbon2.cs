﻿using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Controller;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Ribbon;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;
using Word = Microsoft.Office.Interop.Word;
using Timer = System.Threading.Timer;
using System.Threading;

namespace MeetingPlugin
{
    public partial class Ribbon2
    {
        Outlook.Inspector _meetingInspector;
        private Outlook.Inspector MeetingInspector
        {
            get
            {
                if (_meetingInspector == null)
                {
                    _meetingInspector= Context as Outlook.Inspector;
                }

                return _meetingInspector;
            }
        }

        private Outlook.AppointmentItem MeetingItem { get; set; }
        private bool FooterInjected = false;
        private static FooterEntity Footer;
        private static Timer PropChangeTimer { get; set; }

        private static string HtmlFile
        {
            get
            {
                string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Sustainably Run Meetings plugin\\";
                if (Directory.Exists(folder) == false)
                {
                    Directory.CreateDirectory(folder);
                }

                var file = folder + "\\footer.html";                
                return file;
            }
        }

        private async void Ribbon2_Load(object sender, RibbonUIEventArgs e)
        {
            try
            {
                Log.Info($"Ribbon2_Load - Option.IsActive - ({Option.IsActive})");
                BtnIgnore.Visible = false;

                if (Globals.ThisAddIn.UserInfo != null && !Globals.ThisAddIn.UserInfo.webPluginEnabled) //(Option.IsActive)
                {
                    Log.Info($"Ribbon2_Load - Enabling Desktop... ");
                    try
                    {
                        Globals.ThisAddIn.Monitor.IsIgnore = false;
                        if (MeetingInspector?.CurrentItem is Outlook.AppointmentItem meeting)
                        {
                            var status = meeting.MeetingStatus;
                            meeting.PropertyChange += Meeting_PropertyChange;
                            PropChangeTimer = new Timer(new TimerCallback(PropChangeTick), null, Timeout.Infinite, Timeout.Infinite);
                            if (status == Outlook.OlMeetingStatus.olNonMeeting)
                            {
                                meeting.PropertyChange += Meeting_PropertyChange;
                            }

                            if (meeting.IsRecurring == true)
                            {
                                RemoveFooter();
                            }

                            if (string.IsNullOrEmpty(meeting.EntryID) == false)
                                return;

                            if (status != Outlook.OlMeetingStatus.olNonMeeting)
                            {
                                MeetingItem = meeting;
                                Log.Info($"Ribbon2_Load - Calling Load Meeting... ");
                                await LoadMeeting();
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Log.Error(ex, "Ribbon2_Load");
                    }
                    finally
                    {
                        if (MeetingItem != null)
                        {
                            Marshal.ReleaseComObject(MeetingItem);
                            MeetingItem = null;
                        }
                        //if (inspector != null) Marshal.ReleaseComObject(inspector);
                    }
                }
                else
                {
                    Log.Info($"Ribbon2_Load - Disabling Desktop... ");
                    BtnInfo.Visible = false;
                    BtnIgnore.Visible = false;
                }
                Log.Info($"Ribbon2_Load - Completed");
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "Ribbon2_Load");
            }
        }

        private void PropChangeTick(object state)
        {
            PropChangeTimer.Change(Timeout.Infinite, Timeout.Infinite);
            if (MeetingInspector?.CurrentItem is Outlook.AppointmentItem meeting)
            {
                //MeetingItem = meeting;
                if (meeting.IsRecurring == true && FooterInjected == true)
                {
                    RemoveFooter();
                }
                else if (meeting.IsRecurring == false && FooterInjected == false && !BtnIgnore.Checked)
                {
                    if (Footer != null && string.IsNullOrEmpty(Footer.Footer) == false)
                    {
                        EditDocument(Footer);
                    }
                }
                meeting.PropertyChange += Meeting_PropertyChange;
            }
 
        }

        private void Meeting_Write(ref bool cancel)
        {
            if (BtnIgnore.Checked ==false)
            {
                // add text 


            }
        }

        private bool handle = false;
        public void EditDocument(FooterEntity footer)
        {
            try
            {
                if (footer.Footer == null)
                {
                    FooterInjected = false;
                    return;
                }
                var inspector = ThisAddIn.theCurrentAppointment; //Globals.ThisAddIn.Application.ActiveInspector();
                if (inspector != null && inspector.WordEditor is Word.Document wdDoc)
                {
                    if (File.Exists(HtmlFile))
                    {
                        File.Delete(HtmlFile);
                    }
                    File.WriteAllText(HtmlFile, $"<html> <meta charset=\"utf-8\"><p style = \"color: white; \"><----</p>{footer.Footer}<p style = \"color: white; \">----></p></html>");
                    
                    wdDoc.Paragraphs.Last.Range.InsertParagraphAfter();
                    var paragraph = wdDoc.Paragraphs.Last;
                    paragraph.Range.InsertFile(FileName: HtmlFile, ConfirmConversions: false, Attachment: false);
                    //paragraph.Range.InsertBefore("---");

                    Marshal.ReleaseComObject(paragraph);
                    Marshal.ReleaseComObject(wdDoc);
                }
                FooterInjected = true;
            }
            catch (System.Exception ex)
            {
                Log.Error(ex,"Error while Write Footer-(EditDocument)");
            }
           
        }
        public void RemoveFooter()
        {
            var inspector = ThisAddIn.theCurrentAppointment;
            if (inspector != null && inspector.WordEditor is Word.Document wdDoc)
            {
                bool footerFound = false;
                foreach(Word.Paragraph para in wdDoc.Paragraphs)
                {
                    if (para.Range.Text.Contains("--->"))
                    {
                        footerFound = false;
                        para.Range.Delete();
                    }
                    else if (para.Range.Text.Contains("<---") || footerFound)
                    {
                        footerFound = true;
                        para.Range.Delete();
                    } 
                    else
                    {
                        continue;
                    }
                }
                Word.Range lastRange = wdDoc.Paragraphs.Last.Range;
                if (lastRange.Text == "\r")
                {
                    lastRange.Delete();
                }
            }
            FooterInjected = false;
        }

        private async Task LoadMeeting()
        {
            BtnIgnore.Visible = true;

            string mail = MeetingItem.GetOrganizer().GetSmtpAddress();
            if (!(ExtraControler.IsValid(mail)))
            {
                mail = MeetingItem.SendUsingAccount.SmtpAddress;
            }
            Outlook.UserProperties userProperties = MeetingItem.UserProperties;
            try
            {

                if (userProperties["SustainableDesktopMeeting"] == null)
                {
                    userProperties.Add("SustainableDesktopMeeting", Outlook.OlUserPropertyType.olYesNo, true);
                    userProperties["SustainableDesktopMeeting"].Value = true;
                }
                    
                var footer = await Option.GetFooter(mail);
                if (footer != null && string.IsNullOrEmpty(footer.Footer) == false)
                {
                    EditDocument(footer);
                    Footer = footer;
                    //MeetingItem.AfterWrite += () =>
                    //{
                    //    if(handle) return;
                    //    handle = true;
                    //    Thread t = new Thread(()=> EditDocument(footer));
                    //    t.Start();
                    //    t.Join();
                    //};
                }
            }
            catch(System.Exception ex)
            {
                Log.Error(ex, "Error while LoadMeeting..");
            }
            finally
            {
                if (userProperties != null)
                {
                    Marshal.ReleaseComObject(userProperties);
                    userProperties = null;
                }
            }
        }

        


        private async void Meeting_PropertyChange(string name)
        {
            try
            {
                if (name == "MeetingStatus" || name == "IsRecurring")
                {
                    if (MeetingInspector?.CurrentItem is Outlook.AppointmentItem meeting)
                    {
                        MeetingItem = meeting;
                        if(MeetingItem.IsRecurring)
                        {
                            RemoveFooter();
                        }
                        else
                        {
                            if (MeetingItem.MeetingStatus != Outlook.OlMeetingStatus.olNonMeeting
                                && MeetingItem.MeetingStatus != Outlook.OlMeetingStatus.olMeetingCanceled 
                                && !FooterInjected && !BtnIgnore.Checked)
                            {
                                //await LoadMeeting();
                                if (Footer != null && string.IsNullOrEmpty(Footer.Footer) == false)
                                {
                                    EditDocument(Footer);
                                }
                            }
                        }
                        
                    }
                }
                if (name == "MeetingStatus" || name == "IsRecurring")
                {
                    PropChangeTimer.Change(new TimeSpan(0, 0, 3), new TimeSpan(0, 0, 3));
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex,"Meeting_PropertyChange");
            }
            finally
            {
                if (MeetingItem != null && (name == "MeetingStatus" || name == "IsRecurring"))
                {
                    Marshal.ReleaseComObject(MeetingItem);
                    MeetingItem = null;
                }
            }
        }

        private void BtnInfo_Click(object sender, RibbonControlEventArgs e)
        {
            Process.Start(Globals.ThisAddIn.Monitor.UserInfo.Url);
        }

        private void BtnIgnore_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Globals.ThisAddIn.Monitor.IsIgnore = BtnIgnore.Checked;

                if (BtnIgnore.Checked)
                {
                    RemoveFooter();
                }
                else
                {
                    if (MeetingInspector?.CurrentItem is Outlook.AppointmentItem meeting)
                    {
                        MeetingItem = meeting;
                    }
                    if (!MeetingItem.IsRecurring)
                    {
                        LoadMeeting();
                    }

                    //EditDocument(null);
                }
                
            }
            catch (System.Exception ex)
            {
                Log.Error(ex,"BtnIgnore_Click");
            }
            finally
            {
                if (MeetingItem != null)
                {
                    Marshal.ReleaseComObject(MeetingItem);
                    MeetingItem = null;
                }
            }
        }




    }
}
