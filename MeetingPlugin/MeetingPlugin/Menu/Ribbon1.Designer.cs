﻿namespace MeetingPlugin
{
    partial class Ribbon1 : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon1()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMail = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.BtnInfo = this.Factory.CreateRibbonButton();
            this.tabCalendar = this.Factory.CreateRibbonTab();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.BtnInfo1 = this.Factory.CreateRibbonButton();
            this.tabMeeting = this.Factory.CreateRibbonTab();
            this.group3 = this.Factory.CreateRibbonGroup();
            this.BtnInfo2 = this.Factory.CreateRibbonButton();
            this.tabMail.SuspendLayout();
            this.group1.SuspendLayout();
            this.tabCalendar.SuspendLayout();
            this.group2.SuspendLayout();
            this.tabMeeting.SuspendLayout();
            this.group3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMail
            // 
            this.tabMail.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tabMail.ControlId.OfficeId = "TabMail";
            this.tabMail.Groups.Add(this.group1);
            this.tabMail.Label = "TabMail";
            this.tabMail.Name = "tabMail";
            // 
            // group1
            // 
            this.group1.Items.Add(this.BtnInfo);
            this.group1.Label = "Sustainably Run Meetings";
            this.group1.Name = "group1";
            // 
            // BtnInfo
            // 
            this.BtnInfo.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnInfo.Image = global::MeetingPlugin.Properties.Resources.aboutUs;
            this.BtnInfo.Label = "Info";
            this.BtnInfo.Name = "BtnInfo";
            this.BtnInfo.ShowImage = true;
            this.BtnInfo.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnInfo_Click);
            // 
            // tabCalendar
            // 
            this.tabCalendar.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tabCalendar.ControlId.OfficeId = "TabCalendar";
            this.tabCalendar.Groups.Add(this.group2);
            this.tabCalendar.Label = "TabCalendar";
            this.tabCalendar.Name = "tabCalendar";
            // 
            // group2
            // 
            this.group2.Items.Add(this.BtnInfo1);
            this.group2.Label = "Sustainably Run Meetings";
            this.group2.Name = "group2";
            // 
            // BtnInfo1
            // 
            this.BtnInfo1.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnInfo1.Image = global::MeetingPlugin.Properties.Resources.aboutUs;
            this.BtnInfo1.Label = "Info";
            this.BtnInfo1.Name = "BtnInfo1";
            this.BtnInfo1.ShowImage = true;
            this.BtnInfo1.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnInfo_Click);
            // 
            // tabMeeting
            // 
            this.tabMeeting.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tabMeeting.ControlId.OfficeId = "TabCalendarTableView";
            this.tabMeeting.Groups.Add(this.group3);
            this.tabMeeting.Label = "TabCalendarTableView";
            this.tabMeeting.Name = "tabMeeting";
            // 
            // group3
            // 
            this.group3.Items.Add(this.BtnInfo2);
            this.group3.Label = "Sustainably Run Meetings";
            this.group3.Name = "group3";
            // 
            // BtnInfo2
            // 
            this.BtnInfo2.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnInfo2.Image = global::MeetingPlugin.Properties.Resources.aboutUs;
            this.BtnInfo2.Label = "Info";
            this.BtnInfo2.Name = "BtnInfo2";
            this.BtnInfo2.ShowImage = true;
            this.BtnInfo2.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnInfo_Click);
            // 
            // Ribbon1
            // 
            this.Name = "Ribbon1";
            this.RibbonType = "Microsoft.Outlook.Explorer";
            this.Tabs.Add(this.tabMail);
            this.Tabs.Add(this.tabCalendar);
            this.Tabs.Add(this.tabMeeting);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.tabMail.ResumeLayout(false);
            this.tabMail.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.tabCalendar.ResumeLayout(false);
            this.tabCalendar.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            this.tabMeeting.ResumeLayout(false);
            this.tabMeeting.PerformLayout();
            this.group3.ResumeLayout(false);
            this.group3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tabMail;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnInfo;
        private Microsoft.Office.Tools.Ribbon.RibbonTab tabCalendar;
        private Microsoft.Office.Tools.Ribbon.RibbonTab tabMeeting;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnInfo1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnInfo2;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon1 Ribbon1
        {
            get { return this.GetRibbon<Ribbon1>(); }
        }
    }
}
