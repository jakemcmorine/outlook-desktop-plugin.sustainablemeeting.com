﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MeetingPlugin.Code.Controller;

namespace MeetingPlugin.Code.Forms
{
    public partial class FormUpdate : Form
    {
        private string version = "";
        public FormUpdate(Version dbVersion,Version currentVersion)
        {
            InitializeComponent();

            txtCurrent.Text = $"Your version is: {currentVersion}";
            txtDbVersion.Text = $"Latest version is: {dbVersion}";

            version = dbVersion+"";

            TempPath= $"{ Path.GetTempPath()}{DateTime.Now.ToFileTime()}.msi"; 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        string TempPath{get ;set;}

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string url = $"https://plugin-upload.s3-eu-west-1.amazonaws.com/Setup.{version}.msi";
            //string url = $"http://localhost:8080/app/download/Setup.{version}.msi";

            Thread thread = new Thread(() =>
            {
                WebClient client = new WebClient();
                client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
                client.DownloadFileAsync(new Uri(url), TempPath);
            });
            thread.Start();
        }


        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            this.BeginInvoke((MethodInvoker) delegate {
                double bytesIn = double.Parse(e.BytesReceived.ToString());
                double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
                double percentage = bytesIn / totalBytes * 100;
                progressBar1.Value = int.Parse(Math.Truncate(percentage).ToString());
            });
        }

        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                DialogResult dialogResult = DialogResult.Ignore;
                this.BeginInvoke((MethodInvoker) delegate
                {
                    progressBar1.Value = 100;
                });

                if (File.Exists(TempPath))
                {

                    Process process = new Process();
                    process.StartInfo.FileName = TempPath;
                    process.StartInfo.Arguments = "/passive";
                    process.Start();

                    Thread.Sleep(1000);
                    var processList =Process.GetProcessesByName("msiexec");
                    if (processList.Length > 0)
                    {
                        var process1 = processList.FirstOrDefault(c=>(c.MainWindowTitle+"").Contains("Sustainably Run Meetings"));
                        while (process1?.HasExited==false)
                        {
                            Application.DoEvents();
                            Thread.Sleep(250);
                        }
                    }

                    this.BeginInvoke((MethodInvoker) delegate
                    {
                        dialogResult =  MessageBox.Show(this,"New Version has been installed successfully. \n" +
                                                        "You must restart Outlook for the changes to take effect.");
                    });

                    while (dialogResult != DialogResult.OK)
                    {
                        Thread.Sleep(250);
                    }
                }
                this.Close();
                //var processes = Process.GetProcessesByName("OUTLOOK");
                //if (processes.Count() > 0)
                //{
                //    foreach (Process process in processes)
                //    {
                //        process.Kill();
                //    }
                //}
            }
            catch (Exception ex)
            {
                Log.Error(ex,"client_DownloadFileCompleted");
            }
        }
    }
}
