﻿using MeetingPlugin.Code.API.Controller;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Controller;
using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace MeetingPlugin.Code.Helper
{
    public class OutlookMonitor
    {
        private Outlook.Application Application => Globals.ThisAddIn.Application;
        public CalendarController CalendarController { get; set; }

        private List<Outlook.MAPIFolder> Folders { get; set; }
        private List<Outlook.Folder> InnerFolders { get; set; }
        List<Outlook.Items> Items { get; set; }
        List<string> UserEmails { get; set; }
        public UserEntity UserInfo { get; set; }
        public List<string> CacheNewId { get; set; }

        public List<string> CacheDeletedIds { get; set; }

        public List<string> CacheChangeIds { get; set; }
        private bool IsExcute { get; set; }


        public OutlookMonitor()
        {
            Folders = new List<Outlook.MAPIFolder>();
            Items = new List<Outlook.Items>();
            InnerFolders = new List<Outlook.Folder>();
            CalendarController = new CalendarController();
            UserEmails = new List<string>();
            CacheNewId = new List<string>();
            CacheDeletedIds = new List<string>();
            CacheChangeIds = new List<string>();
        }

        private bool EmptyAccount { get; set; }

        public async Task Initialize()
        {
            Log.Info(".....{ Initialize Plugin }.....");
            CategoryController.CreateCategory();
            InitializeAccount();
            //InitializeDelete();
            Application.NewMailEx += Application_NewMailEx;
            Application.ItemSend += Application_ItemSend;
            UserInfo = await Option.GetInfo(UserEmails);

            //send log for launcher 
            LogController logController = new LogController();
            await logController.CreateLaunchLog(UserEmails);
        }


        public void Exit()
        {
            foreach (var folder in Folders)
            {
                try
                {
                    Marshal.ReleaseComObject(folder);
                }
                catch (System.Exception ex)
                {
                }
            }

            foreach (var item in Items)
            {
                try
                {
                    Marshal.ReleaseComObject(item);
                }
                catch
                {
                }
            }

            foreach (var folder in InnerFolders)
            {
                try
                {
                    Marshal.ReleaseComObject(folder);
                }
                catch (System.Exception)
                {
                }
            }

        }

        private void InitializeAccount()
        {
            foreach (Outlook.Account account in Application.Session.Accounts)
            {
                try
                {
                    string email = "";
                    Outlook.AddressEntry accountEmail = account.CurrentUser.AddressEntry;
                    if (accountEmail != null)
                    {
                        if (accountEmail.AddressEntryUserType ==
                            Outlook.OlAddressEntryUserType.olExchangeUserAddressEntry ||
                            accountEmail.AddressEntryUserType ==
                            Outlook.OlAddressEntryUserType.olExchangeRemoteUserAddressEntry)
                        {
                            Outlook.ExchangeUser exchUser = accountEmail.GetExchangeUser();
                            if (exchUser != null)
                            {
                                email = exchUser.PrimarySmtpAddress;
                            }
                        }
                        else
                        {
                            email = account.SmtpAddress;
                            if (string.IsNullOrEmpty(email))
                            {
                                email = accountEmail.Address;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(email) &&
                        UserEmails.Contains(email) == false)
                    {
                        UserEmails.Add(email);
                    }
                    else
                    {
                        EmptyAccount = true;
                    }

                    var folder = account.DeliveryStore.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);
                    //var deletedItemsFolder = account.DeliveryStore.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderDeletedItems);

                    Folders.Add(folder);
                    //Folders.Add(deletedItemsFolder);
                    var items1 = folder.Items;
                    //var delItem = deletedItemsFolder.Items;

                    items1.ItemAdd += Items_ItemAdd;
                    items1.ItemChange += Items_ItemChange;
                    items1.ItemRemove += Items_ItemDelete;

                    //items1.ItemAdd += SentMail_ItemAdd;

                    //delItem.ItemAdd += DeletedMail_ItemAdd;
                    Items.Add(items1);
                    //Items.Add(delItem);
                }
                catch (System.Exception ex)
                {
                    Log.Error(ex, "error while Initialize Account loop..");
                }
            }
            if (EmptyAccount)
            {
                Application.ItemSend += (object item, ref bool cancel) =>
                {
                    try
                    {
                        string email = "";
                        if (item is Outlook.MeetingItem meeitng)
                        {
                            email = meeitng.SenderEmailAddress;
                        }

                        if (!string.IsNullOrEmpty(email) &&
                            UserEmails.Contains(email) == false)
                        {
                            UserEmails.Add(email);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Log.Error(ex, "Application.ItemSend");
                    }
                };
            }
        }

        private async void DeletedMail_ItemAdd(object Item)
        {
            Outlook.AppointmentItem appointmentItem = null;
            try
            {
                if (Item is Outlook.MeetingItem meeting)
                {
                    if (meeting.MessageClass.Contains("IPM.Schedule.Meeting.Resp"))
                        return;
                    appointmentItem = meeting.GetAssociatedAppointment(false);
                }

                if (Item is Outlook.AppointmentItem appointment)
                {
                    appointmentItem = appointment;
                }

                if (appointmentItem != null && appointmentItem.Start > DateTime.Now)
                {
                    if (!(CacheDeletedIds.Contains(appointmentItem.GlobalAppointmentID)))
                    {
                        await CalendarController.Cancel(appointmentItem);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "DeletedMail_ItemAdd ... ");
            }
            finally
            {
                if (appointmentItem != null)
                {
                    Marshal.ReleaseComObject(appointmentItem);
                    appointmentItem = null;
                }
            }
        }

        private void InitializeDelete()
        {
            try
            {
                Outlook.CalendarModule calModule = (Outlook.CalendarModule)Application.ActiveExplorer().NavigationPane.Modules.GetNavigationModule(Outlook.OlNavigationModuleType.olModuleCalendar);
                foreach (Outlook.NavigationGroup group in calModule.NavigationGroups)
                {
                    for (int i = 1; i <= group.NavigationFolders.Count; i++)
                    {
                        try
                        {
                            Outlook.NavigationFolder folder = group.NavigationFolders[i];
                            Folders.Add(folder.Folder);
                            var items = folder.Folder.Items;
                            items.ItemAdd += Items_ItemAdd;
                            items.ItemChange += Items_ItemChange;
                            items.ItemRemove += Items_ItemDelete;
                            Items.Add(items);

                            if (folder.Folder is Outlook.Folder meetingFolder)
                            {
                                meetingFolder.BeforeItemMove += _meetingFolder_BeforeItemMove;
                                InnerFolders.Add(meetingFolder);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            Log.Error(ex, "InitializeDelete loop... ");
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "InitializeDelete");
            }
        }

        public async void CalendarUpdateTimer(object state)
        {
            //Outlook.NavigationFolder folder = null;
            Outlook.MAPIFolder folder = null;
            Outlook.CalendarModule calModule = null;
            Outlook.NavigationGroup group = null;
            Outlook.Items appointmentItems = null;
            Outlook.Account account = null;
            try
            {
                ThisAddIn.CalendarUpdateTimer.Change(Timeout.Infinite, Timeout.Infinite);
                //calModule = (Outlook.CalendarModule)Application.ActiveExplorer().NavigationPane.Modules.GetNavigationModule(Outlook.OlNavigationModuleType.olModuleCalendar);

                //for (int j = 1; j <= calModule.NavigationGroups.Count; j++)
                {
                    //group = calModule.NavigationGroups[j];
                    //for (int i = 1; i <= group.NavigationFolders.Count; i++)
                    for (int acc = 1; acc <= Application.Session.Accounts.Count; acc++)
                    {
                        try
                        {
                            account = Application.Session.Accounts[acc];

                            //folder = group.NavigationFolders[i];
                            folder = account.DeliveryStore.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);
                            string calName = folder.Name;

                            #region PluginStart Code
                            if (ThisAddIn.PluginStart)
                            {
                                string filter = "[Start] > '" + DateTime.Now.ToString("g") + "'";
                                //appointmentItems = folder.Folder.Items.Restrict(filter);
                                appointmentItems = folder.Items.Restrict(filter);
                                for (int apntCount = 1; apntCount <= appointmentItems.Count; apntCount++)
                                {
                                    AppointmentItem apnt = appointmentItems[apntCount] as AppointmentItem;
                                    try
                                    {
                                        if (apnt != null)
                                        {
                                            //string location = apnt.Location;
                                            UserProperties userProperties = null;
                                            try
                                            {
                                                userProperties = apnt.UserProperties;
                                                //if (userProperties["SustainableMeetingIgnore"] != null)
                                                //{
                                                //    //this is a meeting which the user has opted out.
                                                //    continue;
                                                //}
                                                string meetingOrganizerAddress = apnt.GetSmtpAddress();
                                                if (!(ExtraControler.IsValid(meetingOrganizerAddress)))
                                                {
                                                    meetingOrganizerAddress = apnt.SendUsingAccount.SmtpAddress;
                                                }
                                                if (UserEmails.Contains(meetingOrganizerAddress))
                                                {
                                                    var entity = await apnt.ToEntity();
                                                    if (userProperties["SustainableMeetingId"] != null)
                                                    {
                                                        entity.SMID = (string)userProperties["SustainableMeetingId"].Value;
                                                    }
                                                    ThisAddIn.meetingObjectsList.Add(entity);
                                                    //        string calenderOwner = GetCalendarOwner(apnt);
                                                    //        if (calenderOwner != "" && calenderOwner != meetingOrganizerAddress)
                                                    //        {
                                                    //            continue;
                                                    //        }

                                                    //        if (userProperties["SustainableMeetingId"] != null)
                                                    //        {
                                                    //            //await CalendarController.Edit(apnt);
                                                    //        }
                                                    //        else
                                                    //        {
                                                    //            //await CalendarController.Add(apnt);
                                                    //        }

                                                }
                                            }
                                            finally
                                            {
                                                Marshal.ReleaseComObject(userProperties);
                                                userProperties = null;
                                            }

                                        }
                                    }
                                    finally
                                    {
                                        Marshal.ReleaseComObject(apnt);
                                        apnt = null;
                                    }
                                }

                                //var items = folder.Folder.Items;
                                //var items = folder.Items;
                                //items.ItemAdd += Items_ItemAdd;
                                //items.ItemChange += Items_ItemChange;
                                //items.ItemRemove += Items_ItemDelete;
                                //Items.Add(items);

                                Marshal.ReleaseComObject(appointmentItems);
                                appointmentItems = null;
                            }
                            else
                            #endregion
                            {
                                folder = account.DeliveryStore.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);
                                
                                // filter logic moved inside to cater for recurring appts also.
                                //string filter = "[Start] > '" + DateTime.Now.ToString("g") + "'";
                                //appointmentItems = folder.Items.Restrict(filter);
                                appointmentItems = folder.Items;
                                for (int apntCount = 1; apntCount <= appointmentItems.Count; apntCount++)
                                {
                                    AppointmentItem apnt = appointmentItems[apntCount] as AppointmentItem;
                                    try
                                    {
                                        if (apnt != null && CacheChangeIds.Contains(apnt.GlobalAppointmentID))
                                        {
                                            string location = apnt.Location;
                                            string globalAppointmentId = apnt.GlobalAppointmentID;
                                            UserProperties userProperties = null;
                                            try
                                            {
                                                //Handling the update event for Outside Meetings like from Mobile device
                                                userProperties = apnt.UserProperties;
                                                //if (userProperties["SustainableMeetingIgnore"] != null)//|| userProperties["SustainableDesktopMeeting"] != null)
                                                //{
                                                //    //this is a meeting which the user has opted out or a desktop meeting.
                                                //    continue;
                                                //}
                                                if (CacheNewId.Contains(globalAppointmentId))
                                                {
                                                    CacheNewId.Remove(globalAppointmentId);
                                                    continue;
                                                }
                                                string meetingOrganizerAddress = apnt.GetSmtpAddress();
                                                if (!(ExtraControler.IsValid(meetingOrganizerAddress)))
                                                {
                                                    meetingOrganizerAddress = apnt.SendUsingAccount.SmtpAddress;
                                                }
                                                if (UserEmails.Contains(meetingOrganizerAddress))
                                                {
                                                    string calenderOwner = GetCalendarOwner(apnt);
                                                    if (calenderOwner != "" && calenderOwner != meetingOrganizerAddress)
                                                    {
                                                        continue;
                                                    }

                                                    if (userProperties["SustainableMeetingId"] != null)
                                                    {
                                                        ////Edit only if StartDatetime is greater than today (no need to check this for Recurrence meeting)
                                                        //if((apnt.Start > DateTime.Now && apnt.IsRecurring == false) || (apnt.IsRecurring == true) )
                                                        //{
                                                        //    await CalendarController.Edit(apnt);
                                                        //}

                                                        await CalendarController.Edit(apnt);
                                                       
                                                    }
                                                }
                                            }
                                            finally
                                            {
                                                CacheChangeIds.Remove(globalAppointmentId);
                                                Marshal.ReleaseComObject(userProperties);
                                                userProperties = null;
                                            }
                                        }
                                    }
                                    finally
                                    {
                                        Marshal.ReleaseComObject(apnt);
                                        apnt = null;
                                    }

                                }
                                Marshal.ReleaseComObject(appointmentItems);
                                appointmentItems = null;
                            }

                        }
                        catch (System.Exception ex)
                        {
                            Log.Error(ex, "CalendarUpdateTimer");
                        }
                        finally
                        {
                            Marshal.ReleaseComObject(account);
                            account = null;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "CalendarUpdateTimer");
            }
            finally
            {
                ThisAddIn.PluginStart = false;
                if (folder != null)
                {
                    Marshal.ReleaseComObject(folder);
                    folder = null;
                }

                if (calModule != null)
                {
                    Marshal.ReleaseComObject(calModule);
                    calModule = null;
                }
                if (group != null)
                {
                    Marshal.ReleaseComObject(group);
                    group = null;
                }
                if (appointmentItems != null)
                {
                    Marshal.ReleaseComObject(appointmentItems);
                    appointmentItems = null;
                }
                if (state != null)
                {
                    Marshal.ReleaseComObject(state);
                    state = null;
                }
                ThisAddIn.CalendarUpdateTimer.Change(new TimeSpan(0, 0, 10), new TimeSpan(0, 0, 10));
            }
        }


        private async void Items_ItemDelete()
        {
            Outlook.MAPIFolder folder = null;
            Outlook.Items appointmentItems = null;
            try
            {
                List<string> foundEntities = new List<string>();
                List<string> notFoundEntities = new List<string>();
                foreach (MeetingEntity entity in ThisAddIn.meetingObjectsList)
                {
                    foreach (Outlook.Account account in Application.Session.Accounts)
                    {
                        try
                        {
                            folder = account.DeliveryStore.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);
                            string filter = "[SustainableMeetingId] = '" + entity.SMID + "'";
                            appointmentItems = folder.Items.Restrict(filter);
                            if (appointmentItems.Count == 0)
                            {
                                if (entity.SMID != null)
                                {
                                    notFoundEntities.Add(entity.SMID);
                                }
                            }
                            else
                            {
                                foundEntities.Add(entity.SMID);
                                break;
                            }
                        }
                        catch (System.Exception ex)
                        {
                            Log.Error(ex, "Items_ItemDelete in Accounts ... ");
                        }
                    }
                }
                foreach (var item in notFoundEntities.Distinct())
                {
                    if (foundEntities.Contains(item))
                        continue;


                    MeetingEntity deleteEntity = ThisAddIn.meetingObjectsList.Where(obj => obj.SMID == item).FirstOrDefault();
                    if (deleteEntity != null)
                    {
                        // Check whether recurring meeting to call different controller.
                        if (string.IsNullOrEmpty(deleteEntity.RecurrenceType) == false)
                        {
                            CalendarController.Cancel(item, deleteEntity.OutlookId, true);
                        }
                        else
                        {
                            CalendarController.Cancel(item, deleteEntity.OutlookId, false);
                        }

                        ThisAddIn.meetingObjectsList.Remove(deleteEntity);
                    }
                }
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
                if (folder != null)
                {
                    Marshal.ReleaseComObject(folder);
                    folder = null;
                }
                if (appointmentItems != null)
                {
                    Marshal.ReleaseComObject(appointmentItems);
                    appointmentItems = null;
                }
            }
        }

        private void _meetingFolder_BeforeItemMove(object Item, MAPIFolder MoveTo, ref bool Cancel)
        {
            if (Item is Outlook.AppointmentItem appointment)
            {
            }
        }

        private async void Items_ItemChange(object item)
        {
            Outlook.AppointmentItem appointmentItem = null;
            try
            {
                if (item is Outlook.MeetingItem meeting)
                {
                    appointmentItem = meeting.GetAssociatedAppointment(false);
                }

                if (item is Outlook.AppointmentItem appointment)
                {
                    appointmentItem = appointment;
                }
                if (appointmentItem != null)
                {
                    string location = appointmentItem.Location;
                    string subject = appointmentItem.Subject;
                    UserProperties userProperties = null;
                    try
                    {
                        userProperties = appointmentItem.UserProperties;
                        //if (userProperties["SustainableMeetingIgnore"] != null)
                        //{
                        //    if ((bool)userProperties["SustainableMeetingIgnore"].Value)
                        //    {
                        //        //This is opt out meeting.
                        //        return;
                        //    }
                        //}
                        if (!(CacheChangeIds.Contains(appointmentItem.GlobalAppointmentID)))
                        {
                            CacheChangeIds.Add(appointmentItem.GlobalAppointmentID);
                        }
                    }
                    finally
                    {
                        Marshal.ReleaseComObject(userProperties);
                        userProperties = null;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "Items_ItemChange ... ");
            }
            finally
            {
                if (appointmentItem != null)
                {
                    Marshal.ReleaseComObject(appointmentItem);
                    appointmentItem = null;
                }
            }
        }
        private string GetCalendarOwner(Outlook.AppointmentItem appointment)
        {

            Outlook.MAPIFolder folder = (Outlook.MAPIFolder)appointment.Parent;
            string email = "";
            #region DifferentMethod
            //Outlook.NameSpace ns = null;
            //Outlook.AddressEntry sender = null;
            //string PR_MAILBOX_OWNER_ENTRYID = @"http://schemas.microsoft.com/mapi/proptag/0x661B0102";
            //Outlook.CalendarModule calModule = (Outlook.CalendarModule)Application.ActiveExplorer().NavigationPane.Modules.GetNavigationModule(Outlook.OlNavigationModuleType.olModuleCalendar);
            //foreach (Outlook.NavigationGroup group in calModule.NavigationGroups)
            //{
            //    for (int i = 1; i <= group.NavigationFolders.Count; i++)
            //    {
            //        try
            //        {
            //            Outlook.NavigationFolder navigationFolder = group.NavigationFolders[i];
            //            Outlook.MAPIFolder mapiFolder = navigationFolder.Folder;

            //            if (Application.Session.CompareEntryIDs(folder.Store.StoreID, mapiFolder.Store.StoreID))
            //            {
            //                //string name  = mapiFolder.Name;
            //                //string addrName = mapiFolder.AddressBookName;
            //                //string fldPath = mapiFolder.FolderPath;
            //                ////string storeOwnerEntryId = mapiFolder.PropertyAccessor.BinaryToString(mapiFolder.PropertyAccessor.GetProperty(PR_MAILBOX_OWNER_ENTRYID)) as string;
            //                //string storeOwnerEntryId = mapiFolder.PropertyAccessor.BinaryToString(mapiFolder.EntryID) as string;
            //                //ns = Application.GetNamespace("MAPI"); // i.e. "MAPI"

            //                //sender = ns.GetAddressEntryFromID(storeOwnerEntryId);
            //                //AddressEntry senderAddr = ns.GetAddressEntryFromID(mapiFolder.EntryID);
            //                //AddressEntry storeAddr = ns.GetAddressEntryFromID(mapiFolder.StoreID);
            //                getUserAddress(mapiFolder.StoreID);
            //            }
            //        }
            //        catch (System.Exception ex)
            //        {
            //            Log.Error(ex, "InitializeDelete loop... ");
            //        }
            //    }
            //}
            #endregion
            foreach (Outlook.Account account in Application.Session.Accounts)
            {
                Outlook.MAPIFolder folder2 = account.DeliveryStore.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);

                if (Application.Session.CompareEntryIDs(folder.Store.StoreID, account.DeliveryStore.StoreID))
                {
                    Outlook.AddressEntry accountEmail = account.CurrentUser.AddressEntry;
                    if (accountEmail != null)
                    {
                        if (accountEmail.AddressEntryUserType ==
                            Outlook.OlAddressEntryUserType.olExchangeUserAddressEntry ||
                            accountEmail.AddressEntryUserType ==
                            Outlook.OlAddressEntryUserType.olExchangeRemoteUserAddressEntry)
                        {
                            Outlook.ExchangeUser exchUser = accountEmail.GetExchangeUser();
                            if (exchUser != null)
                            {
                                email = exchUser.PrimarySmtpAddress;
                            }
                        }
                        else
                        {
                            email = account.SmtpAddress;
                            if (string.IsNullOrEmpty(email))
                            {
                                email = accountEmail.Address;
                            }
                        }
                    }
                    break;
                }
            }
            return email;
        }

        #region NotRequired
        //private string getServerShortName(string entryId)
        //{
        //    byte[] temp = HexToByteArray(entryId.Substring(120, entryId.IndexOf("00", 120) - 120));
        //    string serverShortName = System.Text.Encoding.ASCII.GetString(temp);
        //    return serverShortName;
        //}

        //private string getUserAddress(string entryId)
        //{
        //    string serverShortName = getServerShortName(entryId);
        //    byte[] temp = HexToByteArray(entryId.Substring(120 + serverShortName.Length * 2 + 2));
        //    string userAddress = System.Text.Encoding.ASCII.GetString(temp);
        //    return userAddress;
        //}

        //private byte[] HexToByteArray(string hex)
        //{
        //    byte[] bytes = new byte[hex.Length / 2];

        //    for (int i = 0; i < hex.Length; i += 2)
        //    {
        //        bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
        //    }

        //    return bytes;
        //}
        #endregion
        private async void Items_ItemAdd(object item)
        {
            Outlook.AppointmentItem appointmentItem = null;
            try
            {
                if (item is Outlook.MeetingItem meeting)
                {
                    appointmentItem = meeting.GetAssociatedAppointment(false);
                }

                if (item is Outlook.AppointmentItem appointment)
                {
                    appointmentItem = appointment;
                }

                if (appointmentItem != null && appointmentItem.MeetingStatus != OlMeetingStatus.olNonMeeting)
                {
                    UserProperties userProperties = null;
                    try
                    {
                        userProperties = appointmentItem.UserProperties;
                        //if (userProperties["SustainableMeetingIgnore"] != null)
                        //{
                        //    //this is a meeting which the user has opted out.
                        //    return;
                        //}
                        if (userProperties["SustainableDesktopMeeting"] != null)
                        {
                            if ((bool)userProperties["SustainableDesktopMeeting"].Value)
                            {
                                //This is meeting created in Desktop. No need to add from here.
                                return;
                            }
                        }
                    }
                    finally
                    {
                        Marshal.ReleaseComObject(userProperties);
                        userProperties = null;
                    }

                    //if (IsExcute) return;
                    //IsExcute = true;
                    string meetingnOrganizerAddress = appointmentItem.GetSmtpAddress();
                    if (!(ExtraControler.IsValid(meetingnOrganizerAddress)))
                    {
                        meetingnOrganizerAddress = appointmentItem.SendUsingAccount.SmtpAddress;
                    }

                    if (UserEmails.Contains(meetingnOrganizerAddress))
                    {
                        string calenderOwner = GetCalendarOwner(appointmentItem);
                        if (calenderOwner != "" && calenderOwner != meetingnOrganizerAddress)
                        {
                            return;
                        }
                        if (!appointmentItem.MessageClass.Contains("Canceled"))
                        {
                            // For Normal meeting, check EndTime is future instead of'start' because 'AllDay' have to be passed. 
                            if (appointmentItem.End > DateTime.Now || appointmentItem.IsRecurring)
                            {
                                if (!(CacheNewId.Contains(appointmentItem.GlobalAppointmentID)))
                                {
                                    CacheNewId.Add(appointmentItem.GlobalAppointmentID);
                                }
                                await CalendarController.Add(appointmentItem);
                            }
                        }
                    }

                }

            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "Items_ItemAdd ... ");
            }
            finally
            {
                //IsExcute = false;
                if (appointmentItem != null)
                {
                    Marshal.ReleaseComObject(appointmentItem);
                    appointmentItem = null;
                }
            }
        }

        //

        private async void Application_NewMailEx(string entryId)
        {
            object item = this.Application.Session.GetItemFromID(entryId);
            AppointmentItem appointment1 = null;
            AppointmentItem appointment = null;
            object attachmentItem = null;
            Attachment file = null;
            try
            {
                if (item is Outlook.MeetingItem meeting)
                {
                    try
                    {
                        Log.Info("Meeting Email Processing ... ");
                        var x = meeting.Subject;
                        var cls = meeting.MessageClass;
                        var ll = meeting.Class;
                        var email = meeting.GetSenderEmailAddress();

                        int status = GetStatus(meeting.Subject, meeting.Body);

                        appointment1 = meeting.GetAssociatedAppointment(false);
                        if (appointment1 == null)
                        {
                            file = meeting.Attachments.Cast<Outlook.Attachment>().FirstOrDefault(c => Path.GetExtension((c.FileName + "").ToLower()) == ".ics");
                            if (file != null)
                            {
                                var path = Path.GetTempPath() + $"_{DateTime.Now.ToFileTime()}.ics";
                                file.SaveAsFile(path);

                                try
                                {
                                    attachmentItem = this.Application.Session.OpenSharedItem(path);
                                    if (attachmentItem is Outlook.MeetingItem attachmentMeeting)
                                    {
                                        appointment = attachmentMeeting.GetAssociatedAppointment(false);
                                        if (appointment == null)
                                            return;
                                        ThisAddIn.ResponseStatusList.Add(appointment.GlobalAppointmentID);
                                        //AppController.Queue.Add(new AppEntity(){MeetingId = appointment.EntryID,Email = email,IsDone = false,Operation = OperationEntity.Status});
                                        var resultStatus = await CalendarController.Status(attachmentMeeting, email, status);
                                        Log.Info($"Save Status for {email}: {resultStatus}");

                                        //UpdateResponseStatus(appointment, email, status);
                                        Marshal.ReleaseComObject(attachmentMeeting);
                                        attachmentMeeting = null;
                                    }
                                }
                                catch { }
                            }
                        }

                        else
                        {
                            //var email = meeting.GetSenderEmailAddress();
                            //AppController.Queue.Add(new AppEntity(){MeetingId = meeting.EntryID,Email = email,IsDone = false,Operation = OperationEntity.Status});
                            ThisAddIn.ResponseStatusList.Add(appointment1.GlobalAppointmentID);
                            var result = await CalendarController.Status(meeting, email, status);
                            Log.Info($"Save Status for {email}: {result}");
                            //UpdateResponseStatus(appointment1, email, status);
                        }

                    }
                    catch (System.Exception ex)
                    {
                        Log.Error(ex, "Application_NewMailEx");
                    }
                    finally
                    {
                        Marshal.ReleaseComObject(meeting);
                        meeting = null;
                    }
                }
                else
                {
                    return;
                    if (item is Outlook.MailItem mail)
                    {
                        var c1 = mail.Class;
                        var c2 = mail.MessageClass;

                        int status = GetStatus(mail.Subject, mail.Body);
                        var list = mail.Attachments.Cast<Outlook.Attachment>()
                            .Where(c => Path.GetExtension((c.FileName + "").ToLower()) == ".ics");

                        if (list.Any())
                        {
                            foreach (var file1 in list)
                            {
                                var path = Path.GetTempPath() + $"_{DateTime.Now.ToFileTime()}.ics";
                                file1.SaveAsFile(path);
                                attachmentItem = null;
                                try
                                {
                                    attachmentItem = this.Application.Session.OpenSharedItem(path);
                                    if (attachmentItem is Outlook.MeetingItem attachmentMeeting)
                                    {
                                        appointment = attachmentMeeting.GetAssociatedAppointment(false);
                                        if (appointment == null)
                                            return;
                                        var email = mail.SenderEmailAddress;
                                        var result = await CalendarController.Status(appointment, email, status);
                                        Log.Info($"Save Status for {email}: {result}");
                                    }
                                }
                                catch
                                {
                                }
                                finally
                                {
                                    if (attachmentItem != null)
                                        Marshal.ReleaseComObject(attachmentItem);
                                    Marshal.ReleaseComObject(file);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "Application_NewMailEx");
            }
            finally
            {

                if (attachmentItem != null)
                {
                    Marshal.ReleaseComObject(attachmentItem);
                    attachmentItem = null;
                }

                if (file != null)
                {
                    Marshal.ReleaseComObject(file);
                    file = null;
                }

                if (appointment1 != null)
                {
                    Marshal.ReleaseComObject(appointment1);
                    appointment1 = null;
                }

                if (appointment != null)
                {
                    Marshal.ReleaseComObject(appointment);
                    appointment = null;
                }
                if (item != null)
                {
                    Marshal.ReleaseComObject(item);
                    item = null;
                }
            }
        }

        private void UpdateResponseStatus(AppointmentItem appointment, string email, int status)
        {
            MeetingEntity currentObj = ThisAddIn.meetingObjectsList.Where(meetingObj => meetingObj.OutlookId == appointment.GlobalAppointmentID).FirstOrDefault();
            if (currentObj != null)
            {
                InviteEntity invitee = currentObj.Invites.Where(invit => invit.Email == email).FirstOrDefault();
                if (invitee != null)
                {
                    invitee.meetingResponse = GetResponseStatus(status);
                }
            }
        }

        private OlResponseStatus GetResponseStatus(int status)
        {
            if (status == 1)
            {
                return OlResponseStatus.olResponseAccepted;
            }
            else if (status == 2)
            {
                return OlResponseStatus.olResponseDeclined;
            }
            else if (status == 3)
            {
                return OlResponseStatus.olResponseTentative;
            }
            return OlResponseStatus.olResponseNotResponded;
        }

        private int GetStatus(string txt, string bdy)
        {
            string subject = txt.ToLower();
            string body = bdy != null ? bdy.ToLower() : "";
            if (subject.StartsWith("accepted"))
                return 1;
            if (subject.StartsWith("declined"))
                return 2;
            if (subject.StartsWith("tentatively"))
                return 3;

            if (body.Contains("has accepted"))
                return 1;
            if (body.Contains("has declined"))
                return 2;
            if (body.Contains("has tentatively"))
                return 3;
            if (body.Contains("replied \"Maybe\""))
                return 3;



            if (subject.Contains("accepted"))
                return 1;
            if (subject.Contains("declined"))
                return 2;
            if (subject.Contains("tentatively"))
                return 3;


            return 4;
        }


        /*
        private void _meetingFolder_BeforeItemMove(object item, Outlook.MAPIFolder moveTo, ref bool cancel)
        {
            return;
            if (item is Outlook.AppointmentItem appointmentItem)
             {
                 if (appointmentItem.MeetingStatus != Outlook.OlMeetingStatus.olNonMeeting)
                 {
                     Outlook.Folder deletedItemsFolder = (Outlook.Folder)Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderDeletedItems);
                     if (moveTo == null || moveTo.StoreID == deletedItemsFolder.StoreID)
                     {
                         var result = CalendarController.Cancel(appointmentItem).Result;
                     }
                 }
             }
             else if (item is Outlook.MeetingItem meeting)
             {
                 try
                 {
                     Outlook.Folder deletedItemsFolder = (Outlook.Folder)Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderDeletedItems);
                     if (moveTo == null || moveTo.StoreID == deletedItemsFolder.StoreID)
                     {
                         var result = CalendarController.Cancel(meeting).Result;
                     }
                 }
                 catch (Exception ex)
                 {
                     Log.Error(ex, "_meetingFolder_BeforeItemMove");
                 }
                 finally
                 {
                     Marshal.ReleaseComObject(meeting);
                 }
             }


         }
        //*/

        //private Outlook.AppointmentItem MeetingIetem { get; set; }
        public bool IsIgnore { get; set; }
        public void InitializeMenu(Outlook.AppointmentItem meeting)
        {
            //MeetingIetem = meeting;
            //((Outlook.ItemEvents_10_Event) MeetingIetem).Send += OnMenuSend;

        }
        /*
        private void OnMenuSend(ref bool cancel)
        { 
            return;
            ((Outlook.ItemEvents_10_Event) MeetingIetem).Send -= OnMenuSend;
            if(IsExcute) return;
            IsExcute = true;

            try
            {
                if (IsIgnore)
                {
                    if (string.IsNullOrEmpty(MeetingIetem.EntryID))
                    {
                        LogController logController = new LogController();
                        var x = logController.CreateOptOutLog(new List<string>()
                            {MeetingIetem.GetSmtpAddress()}
                        ).Result;
                    }

                    // ignore plz
                    if (MeetingIetem.UserProperties["SustainableMeetingIgnore"] == null)
                        MeetingIetem.UserProperties.Add("SustainableMeetingIgnore", Outlook.OlUserPropertyType.olYesNo, true);

                    MeetingIetem.UserProperties["SustainableMeetingIgnore"].Value = true;
                    MeetingIetem.Save();
                }
                else
                {
                    //return;
                    bool result = false;
                    if ((MeetingIetem.Subject+"").Contains("GEA#"))
                    {
                        result = CalendarController.Edit(MeetingIetem).Result;
                        return;
                    }

                    if ((MeetingIetem.Categories + "").Contains(CategoryController.CategoriesName))
                    {
                        result = CalendarController.Edit(MeetingIetem).Result;
                        return;
                    }
                    if (MeetingIetem.UserProperties["SustainableMeetingId"] != null)
                    {
                        result = CalendarController.Edit(MeetingIetem).Result;
                        return;
                    }

                    if(MeetingIetem.EntryID==null)
                        result = CalendarController.Add(MeetingIetem).Result;
                }
            }
            finally
            {
                IsExcute = false;
            }
        }
        //*/

        private void Application_ItemSend(object item, ref bool cancel)
        {
            if (IsExcute) return;
            IsExcute = true;
            Outlook.AppointmentItem appointmentItem = null;
            Outlook.UserProperties userProperties = null;
            try
            {
                if (item is Outlook.MeetingItem meeting)
                {
                    appointmentItem = meeting.GetAssociatedAppointment(false);
                }

                if (item is Outlook.AppointmentItem appointment)
                {
                    appointmentItem = appointment;
                }

                if (appointmentItem != null)
                {
                    bool result = false;
                    userProperties = appointmentItem.UserProperties;
                    if (IsIgnore)
                    {
                        if (string.IsNullOrEmpty(appointmentItem.EntryID))
                        {
                            LogController logController = new LogController();
                            string email = appointmentItem.GetSmtpAddress();
                            if (!(ExtraControler.IsValid(email)))
                            {
                                email = appointmentItem.SendUsingAccount.SmtpAddress;
                            }
                            result = logController.CreateOptOutLog(new List<string>() { email }).Result;
                        }

                        if (userProperties["SustainableMeetingIgnore"] == null)
                        {
                            userProperties.Add("SustainableMeetingIgnore", Outlook.OlUserPropertyType.olYesNo, true);
                            userProperties["SustainableMeetingIgnore"].Value = true;

                            //Add 'optout@sustainable-meetings.com' to invitee list
                            appointmentItem.Recipients.Add("opt-out@sustainably.run");

                            appointmentItem.Save();

                            result = CalendarController.Add(appointmentItem, true).Result;
                        }
                    }
                    else if (appointmentItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled)
                    {
                        //Delete the occurence only if master appointment, else update will be called by default (automatically - how?)
                        if (appointmentItem.RecurrenceState == Outlook.OlRecurrenceState.olApptMaster)
                        {
                            string globalApptID = appointmentItem.GlobalAppointmentID; //Cancel removes appointmentItem COM Object from memory. To resolve put temp string value to hold apptID.

                            CacheDeletedIds.Add(globalApptID);

                            result = CalendarController.Cancel(appointmentItem).Result;
                            if (result)
                            {
                                MeetingEntity meetingEntity = ThisAddIn.meetingObjectsList.Where(meetingObj => meetingObj.OutlookId == globalApptID).FirstOrDefault();
                                if (meetingEntity != null)
                                {
                                    ThisAddIn.meetingObjectsList.Remove(meetingEntity);
                                }
                            }
                        }

                    }
                    else
                    {

                        if ((appointmentItem.Categories + "").Contains(CategoryController.CategoriesName))
                        {
                            //result = CalendarController.Edit(appointmentItem).Result;
                        }
                        else if (userProperties["SustainableMeetingId"] != null)
                        {
                            //result = CalendarController.Edit(appointmentItem).Result;
                        }
                        else if (appointmentItem.EntryID == null)
                        {
                            result = CalendarController.Add(appointmentItem, true).Result;

                            //if (userProperties["SustainableDesktopMeeting"] == null)
                            //{
                            //    userProperties.Add("SustainableDesktopMeeting", Outlook.OlUserPropertyType.olYesNo, true);
                            //    userProperties["SustainableDesktopMeeting"].Value = true;
                            //    appointmentItem.Save();
                            //}
                        }
                    }
                }
            }
            finally
            {
                if (userProperties != null)
                {
                    Marshal.ReleaseComObject(userProperties);
                    userProperties = null;
                }

                if (appointmentItem != null)
                {
                    Marshal.ReleaseComObject(appointmentItem);
                    appointmentItem = null;
                }

                if (item != null)
                {
                    Marshal.ReleaseComObject(item);
                    item = null;
                }
                IsExcute = false;
            }
        }
    }
}