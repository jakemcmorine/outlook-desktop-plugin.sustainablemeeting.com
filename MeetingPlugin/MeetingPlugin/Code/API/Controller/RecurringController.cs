﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Controller;
using MeetingPlugin.Code.Database;
using Newtonsoft.Json;

namespace MeetingPlugin.Code.API.Controller
{
    public class RecurringController
    {
        public string Error { get; set; }

         public async Task<MeetingResEntity> Create(MeetingEntity entity)
        {
            //int? meetingId = null;
            MeetingResEntity meeting = null;
            string input = "";
            string json = "";
            string message = "";
            try
            {
                entity.Token = await Option.GetToken();
                input = JsonConvert.SerializeObject(entity,
                    new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });

                json = await API.Post("recurring/create", input);

                if (API.IsSuccessStatus)
                {
                    try
                    {
                        var m = JsonConvert.DeserializeObject<MeetingRoot>(json);
                        meeting = m.Meeting;

                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Parse Recurring Meeting Object");
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
                else
                {
                    try
                    {
                        var val = JsonConvert.DeserializeObject<ApiEntity>(json);
                        Error = val.Error;
                        Log.Info("Create Recurring Meeting Error" + json);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Parse ApiEntity Object =>" + json);
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"Create Recurring Meeting Error");
            }
            finally
            {
                await TrackingController.Add(meeting?.MeetingId, meeting?.OutlookId, "AddRecurring", input, json, message);
                //await TrackingController.Add(entity.MeetingId,meetingId+"","AddRecurring", input, json, message);
            }
            return meeting;
        }

         public async Task<bool> Cancel(int id,string outlookId)
         {
             string input = "";
             string json = "";
             string message = "";

             try
             {
                 input = JsonConvert.SerializeObject(
                     new
                     {
                         Token = await Option.GetToken(),
                         Id = id,
                         OutlookID = outlookId
                     });

                 json = await API.Post("recurring/delete", input);

                 try
                 {
                     var m = JsonConvert.DeserializeObject<ApiEntity>(json);
                     if (API.IsSuccessStatus)
                     {
                         if (m.Code == 0)
                             return true;

                         message = Error = m.Error;
                        
                     }
                     else
                     {
                         message = Error = m.Error;
                         Log.Info("Cancel Recurring Error" + json);
                         json.SaveToFile();
                     }
                 }
                 catch (Exception ex)
                 {
                     Log.Error(ex, "CancelRecurring Parse ApiEntity Object");
                     json.SaveToFile();
                     message = ex.Message;
                 }
             }
             catch (Exception ex)
             {
                 message = ex.Message;
                 Log.Error(ex,"Cancel Recurring Error");
             }
             finally
             {
                 await TrackingController.Add(id+"",outlookId,"Cancel recurring", input, json, message);
             }
            
             return false;
         }

        public async Task<int?> Update(int id, MeetingEntity entity)
        {
            string input = "";
            string json = "";
            string message = "";
            int? OrgnizerId = null;
            try
            {

                input = JsonConvert.SerializeObject(entity,
                    new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });

                json = await API.Post("recurring/update", input);


                if (API.IsSuccessStatus)
                {
                    try
                    {
                        var m = JsonConvert.DeserializeObject<UpdateEntity>(json);
                        OrgnizerId = m?.Organizer?.Id;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Update Recurring Meeting Object");
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
                else
                {
                    try
                    {
                        var val = JsonConvert.DeserializeObject<ApiEntity>(json);
                        message = Error = val.Error;
                        Log.Info("Update Recurring Meeting Error" + json);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Update Parse Recurring ApiEntity Object =>" + json);
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex, "Update Recurring Meeting Error");
            }
            finally
            {
                await TrackingController.Add(id + "", entity.OutlookId, "Edit", input, json, message);
            }

            return OrgnizerId;
        }

    }
}
