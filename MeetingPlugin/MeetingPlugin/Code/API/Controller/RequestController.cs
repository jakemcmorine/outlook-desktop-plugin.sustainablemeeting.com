﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Controller;
using MeetingPlugin.Code.Entity;
using Newtonsoft.Json;

namespace MeetingPlugin.Code.API.Controller
{
    public class RequestController
    {
        public string Error { get; set; }

        public async Task<RequestResEntity> Create(RequestEntity entity)
        {
            string input = JsonConvert.SerializeObject(entity,
                new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

            string json = await API.Post("Meeting/request.php",input);
            RequestResEntity meeting = null;
            

            
            if (API.IsSuccessStatus)
            {
                try
                {
                    var m = JsonConvert.DeserializeObject<RequestResEntityRoot>(json);
                    meeting = m.Meeting;

                }
                catch (Exception ex)
                {
                    Log.Error(ex,"Parse Meeting Object");
                    json.SaveToFile();
                }
            }
            else
            {
                try
                {
                    var val =JsonConvert.DeserializeObject<ApiEntity>(json);
                    Error=val.Error;
                    Log.Info("Create Meeting Error" + json);
                }
                catch (Exception ex)
                {
                    Log.Error(ex,"Parse ApiEntity Object =>" + json);
                    json.SaveToFile();
                }
            }

            return meeting;
        }
    }
}
