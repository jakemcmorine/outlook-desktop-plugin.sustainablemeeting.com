﻿using MeetingPlugin.Code.Controller;
using MeetingPlugin.Code.API.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.API.Controller
{
    public class LogController
    {
        public string Error { get; set; }

        public async Task<bool> CreateLaunchLog(List<string> emails)
        {
            string input = "";
            string json = "";
            string message = "";

            try
            {
                input = JsonConvert.SerializeObject(
                    new
                    {
                        Token = await Option.GetToken(),
                        LogType = "Launch",
                        Emails = emails,
                        Email=string.Join(",",emails)
                    });

                json = await API.Post("create-log", input);

                try
                {
                    var m = JsonConvert.DeserializeObject<ApiEntity>(json);
                    if (API.IsSuccessStatus)
                    {
                        if (m.Code == 0)
                            return true;

                        message = Error = m.Error;
                        
                    }
                    else
                    {
                        message = Error = m.Error;
                        Log.Info("CreateLaunch Log Error" + json);
                        json.SaveToFile();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "CreateLaunchLog() Parse ApiEntity Object");
                    json.SaveToFile();
                    message = ex.Message;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"CreateLaunch Log Error");
            }
            finally
            {
                //--
            }
            
            return false;
        }

        public async Task<bool> CreateOptOutLog(List<string> emails)
        {
            string input = "";
            string json = "";
            string message = "";

            try
            {
                input = JsonConvert.SerializeObject(
                    new
                    {
                        Token = await Option.GetToken(),
                        LogType = "Opt Out",
                        Emails=emails,
                        Email=string.Join(",",emails)
                    });

                json = await API.Post("create-log", input);

                try
                {
                    var m = JsonConvert.DeserializeObject<ApiEntity>(json);
                    if (API.IsSuccessStatus)
                    {
                        if (m.Code == 0)
                            return true;

                        message = Error = m.Error;
                        
                    }
                    else
                    {
                        message = Error = m.Error;
                        Log.Info("CreateOptOut Log Error" + json);
                        json.SaveToFile();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "CreateOptOutLog() Parse ApiEntity Object");
                    json.SaveToFile();
                    message = ex.Message;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"CreateOptOut Log Error");
            }
            finally
            {
                //--
            }
            
            return false;
        }
    }
}
