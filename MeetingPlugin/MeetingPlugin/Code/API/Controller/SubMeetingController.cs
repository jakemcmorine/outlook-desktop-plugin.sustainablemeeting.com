﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Controller;
using MeetingPlugin.Code.Database;
using Newtonsoft.Json;

namespace MeetingPlugin.Code.API.Controller
{
    public class SubMeetingController
    {
        public string Error { get; set; }

         public async Task<int?> Create(MeetingEntity entity)
        {
            int? meetingId = null;
            string input = "";
            string json = "";
            string message = "";
            try
            {
                input = JsonConvert.SerializeObject(entity,
                    new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });

                json = await API.Post("SubMeeting/Create.php", input);

                try
                {
                    var val = JsonConvert.DeserializeObject<ApiEntity>(json);
                    if (string.IsNullOrEmpty(Error)==false)
                    {
                        Error = val.Error;
                        Log.Info("Create Sub Meeting Error" + json);
                    }

                    if (string.IsNullOrEmpty(val.ObjectId)==false)
                    {
                        entity.MeetingId = val.ObjectId;
                        if (int.TryParse(val.ObjectId, out var id))
                        {
                            meetingId = id;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Parse ApiEntity Object =>" + json);
                    json.SaveToFile();
                    message = ex.Message;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"Create Sub Meeting Error");
            }
            finally
            {
                await TrackingController.Add(entity.MeetingId,entity?.OutlookId,"AddSub", input, json, message);
            }
            return meetingId;
        }

          public async Task<int?> Update(int id, MeetingEntity entity)
        {
            string input = "";
            string json = "";
            string message = "";
            int? OrgnizerId = null;
            try
            {
                input = JsonConvert.SerializeObject(
                    new
                    {
                        Token = await Option.GetToken(),
                        Id = id,
                        subject = entity.Subject + "",
                        location = entity.Location + "",
                        start = entity.Start,
                        end = entity.End,
                        type = entity.Type,
                        entity.AllDay,
                        Email = entity.Organiser.Email,
                        OutlookID = entity.OutlookId,
                        invites=entity.Invites
                    });

                json = await API.Post("SubMeeting/Update.php",input);
               
            
                if (API.IsSuccessStatus)
                {
                    try
                    {
                        var m = JsonConvert.DeserializeObject<UpdateEntity>(json);
                        OrgnizerId = m?.Organizer?.Id;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex,"Update() Parse Meeting Object");
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
                else
                {
                    try
                    {
                        var val =JsonConvert.DeserializeObject<ApiEntity>(json);
                        message = Error=val.Error;
                        Log.Info("Update Meeting Error" + json);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex,"Update() Parse ApiEntity Object =>" + json);
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"Update Meeting Error");
            }
            finally
            {
                await TrackingController.Add(id+"",entity.OutlookId,"EditSub", input, json, message);
            }

            return OrgnizerId;
        }

         public async Task<bool> Cancel(int id,string outlookId)
         {
             string input = "";
             string json = "";
             string message = "";

             try
             {
                 input = JsonConvert.SerializeObject(
                     new
                     {
                         Token = await Option.GetToken(),
                         Id = id
                     });

                 json = await API.Post("SubMeeting/Cancel.php", input);

                 try
                 {
                     var m = JsonConvert.DeserializeObject<ApiEntity>(json);
                     if (API.IsSuccessStatus)
                     {
                         if (m.Code == 0)
                             return true;

                         message = Error = m.Error;
                        
                     }
                     else
                     {
                         message = Error = m.Error;
                         Log.Info("Cancel SubMeeting Error" + json);
                         json.SaveToFile();
                     }
                 }
                 catch (Exception ex)
                 {
                     Log.Error(ex, "CancelSubMeeting() Parse ApiEntity Object");
                     json.SaveToFile();
                     message = ex.Message;
                 }
             }
             catch (Exception ex)
             {
                 message = ex.Message;
                 Log.Error(ex,"Cancel SubMeeting Error");
             }
             finally
             {
                 await TrackingController.Add(id+"",outlookId,"CancelSub", input, json, message);
             }
            
             return false;
         }

         public async Task<bool> Status(int id,int value,string email,string outlookId)
         {       
             string input = "";
             string json = "";
             string message = "";
             try
             {
                 input = JsonConvert.SerializeObject(
                     new
                     {
                         Token = await Option.GetToken(),
                         Id = id,
                         value=value,
                         email= email
                     });

                 json = await API.Post("SubMeeting/Status.php",input);
            
                 try
                 {
                     var m = JsonConvert.DeserializeObject<ApiEntity>(json);
                     if (API.IsSuccessStatus)
                     {
                         if (m.Code == 0)
                             return true;
                    
                         message =   Error = m.Error;
                     }
                     else
                     {
                         message = Error = m.Error;
                         Log.Info("Change Sub Status Error" + json);
                         json.SaveToFile();
                     }
                 }
                 catch (Exception ex)
                 {
                     message = ex.Message;
                     Log.Error(ex,"Change Sub Status() Parse ApiEntity Object");
                     json.SaveToFile();
                 }
             }
             catch (Exception ex)
             {
                 message = ex.Message;
                 Log.Error(ex,"Change Sub Status Meeting Error");
             }
             finally
             {
                 await TrackingController.Add(id+"",outlookId,"Change Sub Status", input, json, message);
             }
             return false;
         }

    }
}
