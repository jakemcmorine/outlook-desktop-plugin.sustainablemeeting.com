﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Controller;
using Newtonsoft.Json;

namespace MeetingPlugin.Code.API.Controller
{
    public class VersionController
    {
        public string Error { get; set; }

        public async Task<bool> IsEnable(VersionEntity entity)
        {
            string input = JsonConvert.SerializeObject(
                new
                {
                    Token = await Option.GetToken(),
                    outlook = entity.Outlook,
                    build = entity.Build,
                    version = entity.Version,
                });
            string json = await API.Post("outlook-version", input);
            if (API.IsSuccessStatus)
            {
                try
                {
                    var v = JsonConvert.DeserializeObject<VersionRoot>(json);
                    return v.version.IsEnable;
                }
                catch (Exception ex)
                {
                    Log.Error(ex,"Parse Version Object");
                    json.SaveToFile();
                }
            }
            else
            {
                try
                {
                    var val =JsonConvert.DeserializeObject<ApiEntity>(json);
                    Error=val.Error;
                    Log.Info("Create Meeting Error" + json);
                }
                catch (Exception ex)
                {
                    Log.Error(ex,"Parse ApiEntity Object =>" + json);
                    json.SaveToFile();
                }
            }

            return false;
        }

        public static async Task<string> GetVersion(List<string> emails, string currentVersion)
        {
            string input = JsonConvert.SerializeObject(
                new
                {
                    Token = await Option.GetToken(),
                    Emails = emails,
                    CurrentUserVersion = currentVersion
                });
            string json = await API.Post("app_version",input);
            if (API.IsSuccessStatus)
            {
                try
                {
                    dynamic v = JsonConvert.DeserializeObject<dynamic>(json);
                    return v.version;
                }
                catch (Exception ex)
                {
                    Log.Error(ex,"Parse GetVersion Object");
                    json.SaveToFile();
                }
            }

            return "";
        }

    }
}
