﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Controller;
using MeetingPlugin.Code.Database;
using Newtonsoft.Json;

namespace MeetingPlugin.Code.API.Controller
{
    public class MeetingController
    {
        public string Error { get; set; }

        public async Task<MeetingResEntity> Create(MeetingEntity entity)
        {
            MeetingResEntity meeting = null;
            string input = "";
            string json = "";
            string message = "";
            try
            {
                input = JsonConvert.SerializeObject(entity,
                    new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });

                json = await API.Post("meeting-create", input);

                if (API.IsSuccessStatus)
                {
                    try
                    {
                        var m = JsonConvert.DeserializeObject<MeetingRoot>(json);
                        meeting = m.Meeting;

                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Parse Meeting Object");
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
                else
                {
                    try
                    {
                        var val = JsonConvert.DeserializeObject<ApiEntity>(json);
                        Error = val.Error;
                        Log.Info("Create Meeting Error" + json);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Parse ApiEntity Object =>" + json);
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"Create Meeting Error");
            }
            finally
            {
                await TrackingController.Add(meeting?.MeetingId,meeting?.OutlookId,"Add", input, json, message);
            }
            return meeting;
        }

        public async Task<bool> Cancel(int id,string outlookId)
        {
            string input = "";
            string json = "";
            string message = "";

            try
            {
                input = JsonConvert.SerializeObject(
                    new
                    {
                        Token = await Option.GetToken(),
                        Id = id
                    });

                json = await API.Post("meeting-cancel", input);

                try
                {
                    var m = JsonConvert.DeserializeObject<ApiEntity>(json);
                    if (API.IsSuccessStatus)
                    {
                        if (m.Code == 0)
                            return true;

                        message = Error = m.Error;
                        
                    }
                    else
                    {
                        message = Error = m.Error;
                        Log.Info("Cancel Meeting Error" + json);
                        json.SaveToFile();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "CancelMeeting() Parse ApiEntity Object");
                    json.SaveToFile();
                    message = ex.Message;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"Cancel Meeting Error");
            }
            finally
            {
                await TrackingController.Add(id+"",outlookId,"Cancel", input, json, message);
            }
            
            return false;
        }

        public async Task<int?> Update(int id, MeetingEntity entity)
        {
            string input = "";
            string json = "";
            string message = "";
            int? OrgnizerId = null;
            try
            {
                input = JsonConvert.SerializeObject(
                    new
                    {
                        Token = await Option.GetToken(),
                        Id = id,
                        subject = entity.Subject+"",
                        location = entity.Location+"",
                        start = entity.Start,
                        end = entity.End,
                        type = entity.Type,
                        entity.AllDay,
                        Email=entity.Organiser.Email,
                        OutlookID=entity.OutlookId
                    });

                json = await API.Post("meeting-update",input);
               
            
                if (API.IsSuccessStatus)
                {
                    try
                    {
                        var m = JsonConvert.DeserializeObject<UpdateEntity>(json);
                        OrgnizerId = m?.Organizer?.Id;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex,"Update() Parse Meeting Object");
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
                else
                {
                    try
                    {
                        var val =JsonConvert.DeserializeObject<ApiEntity>(json);
                        message = Error=val.Error;
                        Log.Info("Update Meeting Error" + json);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex,"Update() Parse ApiEntity Object =>" + json);
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"Update Meeting Error");
            }
            finally
            {
                await TrackingController.Add(id+"",entity.OutlookId,"Edit", input, json, message);
            }

            return OrgnizerId;
        }

        public async Task<bool> UpdateInvites(int id,MeetingEntity entity)
        {
            string input = "";
            string json = "";
            string message = "";
            InviteEntity[] invitees = entity.Invites.Where(i => i.meetingResponse != Microsoft.Office.Interop.Outlook.OlResponseStatus.olResponseDeclined).ToArray();
            if (invitees.Length == 0)
                return true;
            try
            {
                input = JsonConvert.SerializeObject(
                    new
                    {
                        Token = await Option.GetToken(),
                        Id = id,
                        invites= invitees,// entity.Invites,
                        Email = entity.Organiser.Email
                    });

                json = await API.Post("update-invite",input);
            
                try
                {
                    var m = JsonConvert.DeserializeObject<ApiEntity>(json);
                    if (API.IsSuccessStatus)
                    {
                        if (m!=null && m.Code == 0)
                            return true;
                    
                        message = Error = m.Error;
                    }
                    else
                    {
                        message =Error = m.Error;
                        Log.Info("Update Invites Error" + json);
                        json.SaveToFile();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex,"UpdateInvites() Parse ApiEntity Object");
                    json.SaveToFile();
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"Update Invites Error");
            }
            finally
            {
                await TrackingController.Add(id+"",entity.OutlookId,"Update Invites", input, json, message);
            }

            return false;
        }

        public async Task<bool> Status(int id,int value,string email,string outlookId)
        {       
            string input = "";
            string json = "";
            string message = "";
            try
            {
                input = JsonConvert.SerializeObject(
                    new
                    {
                        Token = await Option.GetToken(),
                        Id = id,
                        value=value,
                        email= email
                    });

                json = await API.Post("remove-invite",input);


                try
                {
                    var m = JsonConvert.DeserializeObject<ApiEntity>(json);
                    if (API.IsSuccessStatus)
                    {
                        if (m.Code == 0)
                            return true;
                    
                        message =   Error = m.Error;
                    }
                    else
                    {
                        message = Error = m.Error;
                        Log.Info("Change Status Error" + json);
                        json.SaveToFile();
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    Log.Error(ex,"Change Status() Parse ApiEntity Object");
                    json.SaveToFile();
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"Change Status Meeting Error");
            }
            finally
            {
                await TrackingController.Add(id+"",outlookId,"Change Status", input, json, message);
            }
            return false;
        }

        public async Task<FooterEntity> Footer(string email)
        {
            string input = JsonConvert.SerializeObject(new {Token = await Option.GetToken(),Email=email});

            string json = await API.Post("footer",input);

            try
            {
                var m = JsonConvert.DeserializeObject<FooterEntity>(json);

                if (API.IsSuccessStatus)
                {

                    if (m.Code == 0)
                        return m;
                    
                    Error = m.Error;
                }
                else
                {
                    Error = m.Error;
                    Log.Info("Get Footer Error" + json);
                    json.SaveToFile();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"Footer() Error in parse ApiEntity Object");
                json.SaveToFile();
            }
            
            return null;
        }

        public async Task<MeetingInfoEntity> GetMeeting(string outlookId)
        {
            MeetingInfoEntity meetingInfo = null;
            string input = "";
            string json = "";
            string message = "";
            try
            {
                input = JsonConvert.SerializeObject(
                    new
                    {
                        Token = await Option.GetToken(),
                        OutlookID = outlookId
                    });

                json = await API.Post("check-meeting", input);


                if (API.IsSuccessStatus)
                {
                    try
                    {
                        var m = JsonConvert.DeserializeObject<MeetingInfo>(json);
                        meetingInfo = m.Meeting;

                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Parse Meeting Object");
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
                else
                {
                    try
                    {
                        var val = JsonConvert.DeserializeObject<ApiEntity>(json);
                        Error = val.Error;
                        Log.Info("Create Meeting Error" + json);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Parse ApiEntity Object =>" + json);
                        json.SaveToFile();
                        message = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex, "Change Status Meeting Error");
            }

            return meetingInfo;

        }
    }
}
