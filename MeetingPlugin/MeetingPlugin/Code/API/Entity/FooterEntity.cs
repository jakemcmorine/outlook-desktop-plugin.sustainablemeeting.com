﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.API.Entity
{
    public class FooterEntity : ApiEntity
    {
        public string Id { get; set; }
        public string ClientId { get; set; }
        public string ChangeDate { get; set; }
        public string Footer { get; set; }
    }
}
