﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.API.Entity
{
    public class MeetingRoot : ApiEntity
    {
        public MeetingResEntity Meeting { get; set; }
    }

    public class MeetingResEntity
    {
        public string MeetingId { get; set; }
        public string ClientId { get; set; }
        public string OutlookId { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        public string Type { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string Created { get; set; }
        public string State { get; set; }
        public string CreatorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public InviteEntity[] Invites { get; set; }
    }

    public class MeetingInfo : ApiEntity
    {
        public MeetingInfoEntity Meeting { get; set; }
    }

    public class MeetingInfoEntity
    {
        public string OutlookId { get; set; }
        public string organiser { get; set; }
        
        public string[] invites { get; set; }
    }
  

}
