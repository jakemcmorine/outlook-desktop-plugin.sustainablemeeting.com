﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.API.Entity
{
    public class MeetingObject
    {

        public string OutlookId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public string[] Invitess { get; set; }

    }
}
