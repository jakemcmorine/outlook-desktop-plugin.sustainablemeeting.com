﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.API.Entity
{
    public class RequestResEntityRoot : ApiEntity
    {     
        public RequestResEntity Meeting { get; set; }
    }


    public class RequestResEntity
    {
        public string Id { get; set; }
        public string ClientId { get; set; }
        public string MeetingId { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        public string Type { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public object CreatedAt { get; set; }
        public object Cancelled { get; set; }
        public string AllDay { get; set; }
        public string OrganiserFirstName { get; set; }
        public string OrganiserLastName { get; set; }
        public string OrganiserEmail { get; set; }
        public string Invites { get; set; }
        public object message { get; set; }
    }



}
