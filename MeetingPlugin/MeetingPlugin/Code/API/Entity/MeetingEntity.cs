﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MeetingPlugin.Code.API.Entity
{
    public class MeetingEntity : ApiEntity
    {        
        public string Token { get; set; }
        [JsonProperty("subject")]
        public string Subject { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("start")]
        public DateTime Start { get; set; }
        [JsonProperty("end")]
        public DateTime End { get; set; }
        public string AllDay { get; set; }

        [JsonProperty("OutlookID")]
        public string OutlookId { get; set; }

        public string TimeZoneOffset { get; set; }        
        public string TimeZoneName { get; set; }        

        [JsonProperty("organiser")]
        public OrganiserEntity Organiser { get; set; }
        [JsonProperty("invites")]
        public InviteEntity[] Invites { get; set; }

        public string SMID { get; set; }
        // For Sub 
        [JsonProperty("MeetingId")]
        public string MeetingId { get; set; }
        public string SubType { get; set; }
        public bool SameAttendance { get; set; }

        // For Recurring meeting info
        public bool NoEndDate { get; set; }
        public string RecurrenceType { get; set; }
        public int DayOfMonth { get; set; }
        public string[] DaysOfWeek { get; set; }
        public int MonthOfYear { get; set; }
        public int Occurrences { get; set; }
        public int Interval { get; set; }
        public int Instance { get; set; }
        public DateTime PatternStartDate { get; set; }
        public DateTime PatternEndDate { get; set; }
        public bool HasExceptions { get; set; }

        // For Deleted Instances
        public DateTime? OriginalDate { get; set; }
        public bool IsCancelled { get; set; }

        //For updated meeting 'instances' of main appointment
        public List<ApiEntity> SubAppointmentsException = new List<ApiEntity>();

    }

    public class OrganiserEntity
    {
        [JsonProperty("firstname")]
        public string Firstname { get; set; }
        [JsonProperty("lastname")]
        public string Lastname { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("company")]
        public string Company { get; set; }
    }

    public class InviteEntity
    {
        [JsonProperty("firstname")]
        public string Firstname { get; set; }
        [JsonProperty("lastname")]
        public string Lastname { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("company")]
        public string Company { get; set; }

        public Microsoft.Office.Interop.Outlook.OlResponseStatus meetingResponse { get; set; }
    }



}
