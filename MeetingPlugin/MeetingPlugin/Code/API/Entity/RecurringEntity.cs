﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.API.Entity
{
    public class RecurringEntity : ApiEntity
    { 
        public string Token { get; set; }
        public string MeetingId{get;set;}
        public int Duration{get;set;}
        public bool NoEndDate{get;set;}
        public string RecurrenceType{get;set;}
        public int DayOfMonth{get;set;}
        public string[] DaysOfWeek {get;set;}
        public int MonthOfYear{get;set;}
        public int Occurrences{get;set;}
        public int Instance{get;set;}
        public DateTime PatternStartDate{get;set;}
        public DateTime PatternEndDate{get;set;}
        public bool HasExceptions{get;set;}
    }
}
