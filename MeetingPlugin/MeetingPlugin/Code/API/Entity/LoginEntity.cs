﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.API.Entity
{
    public class LoginEntity : ApiEntity
    {
        public string Token { get; set; }
    }
}
