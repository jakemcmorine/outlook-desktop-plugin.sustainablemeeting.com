﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.API.Entity
{
    public class VersionRoot : ApiEntity
    {
        public VersionEntity version { get; set; }
    }

    public class VersionEntity
    {
        public int Id { get; set; }
        public string Outlook { get; set; }
        public string Build { get; set; }
        public string Version { get; set; }
        public bool IsEnable { get; set; }
    }




}
