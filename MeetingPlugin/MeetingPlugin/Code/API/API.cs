﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using MeetingPlugin.Code.Controller;
using System.Net;

namespace MeetingPlugin.Code.API
{
    public class API
    {
        public static int StatusCode { get; set; }
        public static bool IsSuccessStatus { get; set; }

        public static async Task<string> Post(string module, string json = null)
        {
            string url = Option.ServerUrl + module;
            try
            {
                using (var client = new HttpClient())
                {
                    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    client.Timeout = new TimeSpan(1, 0, 0);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(url, content);
                    StatusCode = (int) response.StatusCode;
                    IsSuccessStatus = response.IsSuccessStatusCode;

                    var responseContent = response.Content;
                    string responseString = await responseContent.ReadAsStringAsync();
                    return responseString;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"API.Post({module},{url})");
            }
            return "";
        }




    }
}
