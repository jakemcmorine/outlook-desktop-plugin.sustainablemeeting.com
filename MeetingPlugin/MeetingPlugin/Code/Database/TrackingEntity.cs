﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.Database
{
    public  class TrackingEntity
    {
        public string ServerId { get; set; }
        public string OutlookId { get; set; }
        public string Action { get; set; }
        public string InputData { get; set; }
        public string Output { get; set; }
        public string Message { get; set; }
    }
}
