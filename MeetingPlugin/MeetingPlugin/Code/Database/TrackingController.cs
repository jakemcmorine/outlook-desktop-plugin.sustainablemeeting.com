﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Controller;
using MeetingPlugin.Code.Entity;
using Newtonsoft.Json;
using Api=MeetingPlugin.Code.API.API;

namespace MeetingPlugin.Code.Database
{
    public class TrackingController
    {
        public static string Error { get; set; }
        public static async Task<bool> Add(string id, string outlookId, string action, string inputData, string output, string info)
        {
            MeetingResEntity meeting = null;
            string input = "";
            string json = "";
            string message = "";
            try
            {

                input = JsonConvert.SerializeObject(new
                {
                    Token = await Option.GetToken(),
                    OutlookId = outlookId,
                    InputData = inputData,
                    Action = action,
                    ServerId = id,
                    Output = output,
                    Message = info
                }, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore

                });

                json = await Api.Post("tracking", input);

                    try
                    {
                        var m = JsonConvert.DeserializeObject<ApiEntity>(json);

                        if (Api.IsSuccessStatus)
                        {
                            return true;
                        }
                        else
                        {
                            Error = m.Error;
                            Log.Info("Create Tracking Error" + json);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Parse ApiEntity Object =>" + json);
                        json.SaveToFile();
                        message = ex.Message;
                    }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Error(ex,"Create Tracking Error");
            }

            return false;
        }

    }
}
