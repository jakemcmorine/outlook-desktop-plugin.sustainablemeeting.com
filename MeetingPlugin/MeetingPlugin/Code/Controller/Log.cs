﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace MeetingPlugin.Code.Controller
{
    public class Log
    {
        private static Logger Nlogger => LogManager.GetCurrentClassLogger();


        private static string LogFolder
        {
            get
            {
                string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Sustainably Run Meetings plugin\\log\\";
                if (Directory.Exists(folder) == false)
                {
                    Directory.CreateDirectory(folder);
                }
                return folder;
            }
        }

        private static string LogPath => LogFolder + "App.Log";

        private static void Init()
        {
            if (!Directory.Exists(LogFolder))
            {
                Directory.CreateDirectory(LogFolder);
            }


            var config = new LoggingConfiguration();
            var logfile = new FileTarget() { FileName = LogPath, Name = "logfile", Layout = "${longdate} ${message} ${exception:format=ToString}" };
            config.AddTarget("ErrorLog", logfile);
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Info, logfile));
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Debug, logfile));
            LogManager.Configuration = config;
        }

        public static void Error(Exception ex, string text)
        {
            Init();
            Nlogger.Error(ex, text);
        }

        public static void Info(string text)
        {
            Init();
            Nlogger.Info(text);
        }

    }
}
