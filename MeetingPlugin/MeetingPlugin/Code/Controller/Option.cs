﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MeetingPlugin.Code.API.Controller;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.API;
using Newtonsoft.Json;
using Api = MeetingPlugin.Code.API.API;

namespace MeetingPlugin.Code.Controller
{
    public class Option
    {
        //public static string ServerUrl => "http://localhost:8080/app/";
        //public static string ServerUrl => "https://msm20.com/gea/";
        //public static string ServerUrl => "https://greenearthappeal.org/app/";
        //"https://0oydqet963.execute-api.eu-west-1.amazonaws.com/dev/"; //"https://pymuaetb0b.execute-api.eu-west-1.amazonaws.com/prod/";
        //public static string ServerUrl => "https://dev-api-ss.emvigotechnologies.com/";
        public static string ServerUrl => "https://api-meetings.sustainably.run/";

        public static string UserName => "mike@codeofaninja.com";
        public static string Password => "555";
        private static string _token;

        public static bool IsActive { get; set; }

        public static async Task<string> GetToken()
        {
            if (string.IsNullOrEmpty(_token))
            {                
                string response = await Api.Post("login", $"{{\"email\" : \"{UserName}\",\"password\" : \"{Password}\"}}");
                if (Api.IsSuccessStatus)
                {
                    var loginObject = JsonConvert.DeserializeObject<LoginEntity>(response);
                    _token = loginObject.Token;
                }
            }

            return _token;
        }


        private static FooterEntity _footer;

        public static async Task<FooterEntity> GetFooter(string email)
        {
            //if (_footer == null)
            {
                Log.Info("GetFooter() - email: " + email);
                var response = await new MeetingController().Footer(email);
                _footer = response;
                Log.Info("GetFooter() - Completed");
            }
            return _footer;
        }

        public static string Error { get; set; }
        public static async Task<UserEntity> GetInfo(List<string> emails)
        {
            string input = JsonConvert.SerializeObject(
                new
                {
                    Token = await GetToken(),
                    Emails = emails
                });

            string json = await Api.Post("info", input);
            try
            {
                var m = JsonConvert.DeserializeObject<UserEntity>(json);
                if (Api.IsSuccessStatus)
                {
                    if (m.Code == 0)
                        return m;

                    Error = m.Error;
                }
                else
                {
                    Error = m.Error;
                    Log.Info("Info Error" + json);
                    json.SaveToFile();
                }


            }
            catch (Exception ex)
            {
                Log.Error(ex, "Parse UserEntity Object");
                json.SaveToFile();
            }

            return null;
        }

        public static async Task<UserEntity> CheckWebPluginForUser(List<string> emails)
        {
            
            try
            {
                string input = JsonConvert.SerializeObject(
                new
                {
                    Emails = emails
                });
                Log.Info($"Check WebPlugin For User - Inputs ({input})");
                string json = await Api.Post("checkWebPluginForUser", input);
                Log.Info($"Check WebPlugin For User -Output ({json})");

                var m = JsonConvert.DeserializeObject<UserEntity>(json);
                if (Api.IsSuccessStatus)
                {
                    if (m.Code == 0)
                        return m;

                    Error = m.Error;
                }
                else
                {
                    Error = m.Error;
                    Log.Info("Check WebPlugin For User API Error" + json);
                    json.SaveToFile();
                }


            }
            catch (Exception ex)
            {
                Log.Error(ex, "Check Webplugin UserEntity Object");
                //json.SaveToFile();
            }

            return null;
        }


    }
}
