﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace MeetingPlugin.Code.Controller
{
    public class CategoryController
    {
        private static  Outlook.Application Application => Globals.ThisAddIn.Application;
        public static string CategoriesName => "Sustainably Run Meetings";
        
        public static void CreateCategory()
        {
            Outlook.Categories olCategoryList = Application.Session.Categories;
            Outlook.NameSpace ns = Application.GetNamespace("MAPI");
            try
            { 
                bool retValue = true;
                foreach (Outlook.Category category in olCategoryList)
                {
                    if (category.Name.Equals(CategoriesName))
                    {
                        retValue = false;
                        break;
                    }
                }

                if (retValue)
                {
                    ns.Categories.Add(CategoriesName, Outlook.OlCategoryColor.olCategoryColorDarkGreen);
                    Log.Info($"Create New Category {CategoriesName}");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"CategoryController.CreateCategory()");
            }
            finally
            {
                if (olCategoryList != null) Marshal.ReleaseComObject(olCategoryList);
                if (ns != null) Marshal.ReleaseComObject(ns);
            }            
        }
    }
}
