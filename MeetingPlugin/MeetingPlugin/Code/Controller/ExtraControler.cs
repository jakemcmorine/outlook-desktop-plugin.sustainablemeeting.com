﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using MeetingPlugin;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Controller;
using MeetingPlugin.Code.Entity;
using Outlook = Microsoft.Office.Interop.Outlook;

public static class ExtraControler
{
    public static async Task<MeetingEntity> ToEntity(this Outlook.MeetingItem meeting, Outlook.AppointmentItem appointment)
    {
        MeetingEntity entity = new MeetingEntity();
        if (appointment != null)
        {
            try
            {
                entity.Token = await Option.GetToken();
                meeting.Recipients.ResolveAll();
                entity.Subject = meeting.Subject + "";
                entity.Location = appointment.Location;
                entity.OutlookId = appointment.GlobalAppointmentID;
                entity.Start = appointment.StartUTC;
                entity.End = appointment.EndUTC;
                entity.AllDay = appointment.AllDayEvent ? "1" : "0";

                string type = "One Time";
                if (appointment.IsRecurring)
                {
                    var recurrence = appointment.GetRecurrencePattern();

                    switch (recurrence.RecurrenceType)
                    {
                        case Outlook.OlRecurrenceType.olRecursDaily:
                            type = "Recurrence Daily";
                            break;
                        case Outlook.OlRecurrenceType.olRecursWeekly:
                            type = "Recurrence Weekly";
                            break;
                        case Outlook.OlRecurrenceType.olRecursMonthNth:
                            type = "Recurrence Monthly";
                            break;
                        case Outlook.OlRecurrenceType.olRecursMonthly:
                            type = "Recurrence Monthly";
                            break;
                        case Outlook.OlRecurrenceType.olRecursYearNth:
                            type = "Recurrence Yearly";
                            break;
                        case Outlook.OlRecurrenceType.olRecursYearly:
                            type = "Recurrence Yearly";
                            break;
                    }

                    Marshal.ReleaseComObject(recurrence);
                }

                entity.Type = type;

                #region Organizer

                entity.Organiser = new OrganiserEntity();
                entity.Organiser.Email = meeting.GetSmtpAddress();
                if (!(IsValid(entity.Organiser.Email)))
                {
                    entity.Organiser.Email = meeting.SendUsingAccount.SmtpAddress;
                }

                string fname = "";
                string lname = "";
                string title = "";
                string company = "";

                var organizer = appointment.GetOrganizer();
                if (organizer != null)
                {
                    var contact = organizer.GetContact();
                    if (contact == null)
                    {
                        var user = organizer.GetExchangeUser();
                        if (user != null)
                        {
                            fname = user.FirstName;
                            lname = user.LastName;
                            company = user.CompanyName;
                        }
                    }
                    else
                    {
                        title = contact.Title;
                        fname = contact.FirstName;
                        lname = contact.LastName;
                        company = contact.CompanyName;
                    }
                    entity.Organiser.Email = appointment.GetSmtpAddress();
                    if (!(IsValid(entity.Organiser.Email)))
                    {
                        entity.Organiser.Email = appointment.SendUsingAccount.SmtpAddress;
                    }
                }

                if (string.IsNullOrEmpty(fname) && string.IsNullOrEmpty(appointment.Organizer) == false)
                {
                    fname = appointment.Organizer;
                    if (appointment.Organizer.Contains(" "))
                    {
                        var nameParts = appointment.Organizer.Split(' ').ToList();

                        lname = nameParts.LastOrDefault();
                        nameParts.RemoveAt(nameParts.Count - 1);
                        fname = string.Join(" ", nameParts);
                        company = appointment.Companies;
                    }
                }

                entity.Organiser.Title = title;
                entity.Organiser.Firstname = fname;
                entity.Organiser.Lastname = lname;
                entity.Organiser.Company = company;

                #endregion


                #region Invites

                var invites = new List<InviteEntity>();
                foreach (Outlook.Recipient recipient in meeting.Recipients)
                {
                    fname = "";
                    lname = "";
                    title = "";
                    company = "";
                    var invite = new InviteEntity();
                    invite.Email = recipient.GetSmtpAddress();

                    if (invite.Email == entity.Organiser.Email)
                        continue;

                    var addressEntry = recipient.AddressEntry;
                    var contact = addressEntry.GetContact();
                    if (contact == null)
                    {
                        var user = addressEntry.GetExchangeUser();
                        if (user != null)
                        {
                            fname = user.FirstName;
                            lname = user.LastName;
                            company = user.CompanyName;
                        }
                    }
                    else
                    {
                        title = contact.Title;
                        fname = contact.FirstName;
                        lname = contact.LastName;
                        company = contact.CompanyName;
                    }

                    if (string.IsNullOrEmpty(fname))
                    {
                        fname = recipient.Name;
                        if (recipient.Name.Contains(" "))
                        {
                            var nameParts = recipient.Name.Split(' ').ToList();

                            lname = nameParts.LastOrDefault();
                            nameParts.RemoveAt(nameParts.Count - 1);
                            fname = string.Join(" ", nameParts);
                        }
                    }

                    invite.Title = title;
                    invite.Company = company;
                    invite.Firstname = fname;
                    invite.Lastname = lname;

                    invites.Add(invite);
                }
                entity.Invites = invites.ToArray();

                #endregion

            }
            finally
            {

            }
        }

        return entity;
    }


    public static async Task<MeetingEntity> ToEntity(this Outlook.AppointmentItem appointment)
    {
        MeetingEntity entity = new MeetingEntity();
        if (appointment != null)
        {
            try
            {
                var offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now);
                //var offset = TimeZoneInfo.Local.BaseUtcOffset;
                //entity.TimeZoneOffset = $"{offset.TotalHours:+#.#;-#.#;0}";
                entity.TimeZoneOffset = ((offset < TimeSpan.Zero) ? "-" : "+") + offset.ToString();
                entity.TimeZoneName = TimeZoneInfo.Local.StandardName;
                entity.Token = await Option.GetToken();
                //appointment.Recipients.ResolveAll();
                entity.Subject = appointment.Subject + "";
                entity.Location = appointment.Location;
                entity.OutlookId = appointment.GlobalAppointmentID;
                entity.Start = appointment.StartUTC;
                entity.End = appointment.EndUTC;
                entity.AllDay = appointment.AllDayEvent ? "1" : "0";

                string type = "One Time";
                if (appointment.IsRecurring)
                {
                    var recurrence = appointment.GetRecurrencePattern();

                    switch (recurrence.RecurrenceType)
                    {
                        case Outlook.OlRecurrenceType.olRecursDaily:
                            type = "Recurrence Daily";
                            break;
                        case Outlook.OlRecurrenceType.olRecursWeekly:
                            type = "Recurrence Weekly";
                            break;
                        case Outlook.OlRecurrenceType.olRecursMonthNth:
                            type = "Recurrence Monthly";
                            break;
                        case Outlook.OlRecurrenceType.olRecursMonthly:
                            type = "Recurrence Monthly";
                            break;
                        case Outlook.OlRecurrenceType.olRecursYearNth:
                            type = "Recurrence Yearly";
                            break;
                        case Outlook.OlRecurrenceType.olRecursYearly:
                            type = "Recurrence Yearly";
                            break;
                    }

                    //entity.Duration = recurrence.Duration;
                    entity.Interval = recurrence.Interval;
                    entity.Occurrences = recurrence.Occurrences;
                    entity.Instance = recurrence.Instance;
                    entity.DayOfMonth = recurrence.DayOfMonth;
                    entity.MonthOfYear = recurrence.MonthOfYear;
                    //entity.PatternStartDate = recurrence.PatternStartDate;
                    //entity.PatternEndDate = recurrence.PatternEndDate;
                    //entity.PatternStartDate = recurrence.PatternStartDate.ToUniversalTime();
                    //entity.PatternEndDate = recurrence.PatternEndDate.ToUniversalTime();

                    //new DateTime(takeOnlyDate.Year, takeOnlyDate.Month,takeOnlyDate.Day, takeOnlyTime.Hour, takeOnlyTime.Minute, takeOnlyTime.Second);

                    entity.PatternStartDate = DateAndTimeAdd(recurrence.PatternStartDate,appointment.Start).ToUniversalTime();
                    entity.PatternEndDate = DateAndTimeAdd(recurrence.PatternEndDate, appointment.End).ToUniversalTime();


                    entity.NoEndDate = recurrence.NoEndDate;
                    entity.RecurrenceType = Enum.GetName(typeof(Outlook.OlRecurrenceType), recurrence.RecurrenceType);                   
                    entity.DaysOfWeek = GetWeekDaysSelected(recurrence.DayOfWeekMask).ToArray();

                    Marshal.ReleaseComObject(recurrence);
                    recurrence = null;
                }

                entity.Type = type;

                #region Organizer

                entity.Organiser = new OrganiserEntity();
                string fname = "";
                string lname = "";
                string title = "";
                string company = "";

                var organizer = appointment.GetOrganizer();
                if (organizer != null)
                {
                    var contact = organizer.GetContact();
                    if (contact == null)
                    {
                        var user = organizer.GetExchangeUser();
                        if (user != null)
                        {
                            fname = user.FirstName;
                            lname = user.LastName;
                            company = user.CompanyName;
                        }
                    }
                    else
                    {
                        title = contact.Title;
                        fname = contact.FirstName;
                        lname = contact.LastName;
                        company = contact.CompanyName;
                    }

                    entity.Organiser.Email = appointment.GetSmtpAddress();
                    if (!(IsValid(entity.Organiser.Email)))
                    {
                        entity.Organiser.Email = appointment.SendUsingAccount.SmtpAddress;
                    }
                }
                Marshal.ReleaseComObject(organizer);
                organizer = null;

                if (string.IsNullOrEmpty(fname) && string.IsNullOrEmpty(appointment.Organizer) == false)
                {
                    fname = appointment.Organizer;
                    if (appointment.Organizer.Contains(" "))
                    {
                        var nameParts = appointment.Organizer.Split(' ').ToList();

                        lname = nameParts.LastOrDefault();
                        nameParts.RemoveAt(nameParts.Count - 1);
                        fname = string.Join(" ", nameParts);
                        company = appointment.Companies;
                    }
                }

                entity.Organiser.Title = title;
                entity.Organiser.Firstname = fname;
                entity.Organiser.Lastname = lname;
                entity.Organiser.Company = company;

                #endregion


                #region Invites

                var invites = new List<InviteEntity>();
                Outlook.Recipients appointmentRecipients = appointment.Recipients;

                Outlook.Recipient recipient = null;
                try
                {
                    for (int i = 1; i <= appointmentRecipients.Count; i++)
                    //foreach (Outlook.Recipient recipient in appointmentRecipients)
                    {
                        
                        recipient = appointmentRecipients[i];
                        recipient.Resolve();
                        fname = "";
                        lname = "";
                        title = "";
                        company = "";
                        var invite = new InviteEntity();
                        invite.Email = recipient.GetSmtpAddress();

                        if (!(ExtraControler.IsValid(invite.Email)))
                        {
                            continue;
                        }
                        if (invite.Email == entity.Organiser.Email)
                            continue;

                        if (appointment.IsRecurring && recipient.Sendable == false)
                            continue;

                        var addressEntry = recipient.AddressEntry;
                        Outlook.ContactItem contact = null;
                        try
                        {
                            contact = addressEntry.GetContact();
                        }
                        catch (Exception ex)
                        {

                        }
                        if (contact == null)
                        {
                            var user = addressEntry.GetExchangeUser();
                            if (user != null)
                            {
                                fname = user.FirstName;
                                lname = user.LastName;
                                company = user.CompanyName;
                            }
                        }
                        else
                        {
                            title = contact.Title;
                            fname = contact.FirstName;
                            lname = contact.LastName;
                            company = contact.CompanyName;
                        }

                        if (string.IsNullOrEmpty(fname))
                        {
                            fname = recipient.Name;
                            if (recipient.Name.Contains(" "))
                            {
                                var nameParts = recipient.Name.Split(' ').ToList();

                                lname = nameParts.LastOrDefault();
                                nameParts.RemoveAt(nameParts.Count - 1);
                                fname = string.Join(" ", nameParts);
                            }
                        }

                        invite.Title = title;
                        invite.Company = company;
                        invite.Firstname = fname;
                        invite.Lastname = lname;
                        invite.meetingResponse = recipient.MeetingResponseStatus;

                        if(invite.meetingResponse!= Outlook.OlResponseStatus.olResponseDeclined)
                        {
                            invites.Add(invite);
                        }                            
                    }

                    //entity.Invites = invites.ToArray();
                    entity.Invites = invites.GroupBy(x => x.Email).Select(y => y.First()).ToArray(); // Sometimes invitee list is duplicated; case: When invitee cancels appt and then later accepts it.
                }
                finally
                {
                    Marshal.ReleaseComObject(recipient);
                    recipient = null;
                    Marshal.ReleaseComObject(appointmentRecipients);
                    appointmentRecipients = null;
                }
                

                #endregion

            }
            catch (Exception ex)
            {

            }
            finally
            {
                
            }
        }

        return entity;
    }

    public static async Task<MeetingEntity> ToEntity(this Outlook.Row row)
    {
        MeetingEntity entity = new MeetingEntity();
        if (row != null)
        {
            try
            {
                var offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now);
                entity.TimeZoneOffset = $"{offset.TotalHours:+#;-#;0}";
                entity.TimeZoneName = TimeZoneInfo.Local.StandardName;
                entity.Token = await Option.GetToken();
                entity.Subject = (string)row["Subject"];
                entity.Location = (string)row["Location"];
                //entity.OutlookId =  row["GlobalAppointmentID"];
                entity.Start = (DateTime)row["Start"];
                entity.End = (DateTime)row["End"];
                entity.AllDay = (string)row["AllDayEvent"]=="1" ? "1" : "0";

                entity.Start = entity.Start.ToUniversalTime();
                entity.End = entity.End.ToUniversalTime();
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

        return entity;
    }

    public static RecurringEntity ToRecurring(this Outlook.AppointmentItem appointment)
    {
        RecurringEntity entity = new RecurringEntity();
        if (appointment != null)
        {
            Outlook.RecurrencePattern recurrence= null;
            try
            {
                recurrence = appointment.GetRecurrencePattern();
                entity = new RecurringEntity();

                entity.Duration = recurrence.Duration;
                entity.Occurrences = recurrence.Occurrences;
                entity.Instance = recurrence.Instance;
                entity.DayOfMonth = recurrence.DayOfMonth;
                entity.MonthOfYear = recurrence.MonthOfYear;
                entity.PatternStartDate = recurrence.PatternStartDate;
                entity.PatternEndDate = recurrence.PatternEndDate;
                entity.NoEndDate = recurrence.NoEndDate;
                entity.RecurrenceType = Enum.GetName(typeof(Outlook.OlRecurrenceType), recurrence.RecurrenceType); ;
                entity.DaysOfWeek = GetWeekDaysSelected(recurrence.DayOfWeekMask).ToArray();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "ToRecurring");
            }
            finally
            {
                if (recurrence != null) Marshal.ReleaseComObject(recurrence);
            }
        }

        return entity;
    }

    public static DateTime DateAndTimeAdd(DateTime takeOnlyDate, DateTime takeOnlyTime)
    {
        return new DateTime(takeOnlyDate.Year, takeOnlyDate.Month, takeOnlyDate.Day, takeOnlyTime.Hour, takeOnlyTime.Minute, takeOnlyTime.Second);
    }

    public static string GetSmtpAddress(this Outlook.MeetingItem meeting)
    {
        Outlook.Recipient recip = null;
        Outlook.ExchangeUser exUser = null;
        string sAddress = "";
        try
        {
            if (meeting?.SenderEmailType.ToLower() == "ex")
            {
                recip = Globals.ThisAddIn.Application.Session.CreateRecipient(meeting.SenderEmailAddress);
                exUser = recip?.AddressEntry?.GetExchangeUser();
                sAddress = exUser?.PrimarySmtpAddress;
                if (string.IsNullOrEmpty(sAddress))
                {
                    sAddress = meeting?.SenderEmailAddress?.Replace("'", "");
                }
            }
            else
            {
                sAddress = meeting.SenderEmailAddress.Replace("'", "");
            }
        }
        catch (Exception ex)
        {
            Log.Error(ex, "GetSmtpAddress meeting Error");
        }
        finally
        {
            if (exUser != null) Marshal.ReleaseComObject(exUser);
            if (recip != null) Marshal.ReleaseComObject(recip);
        }

        return sAddress;
    }

    public static string GetSmtpAddress(this Outlook.AppointmentItem appointment)
    {
        if (appointment == null) return "";
        Outlook.Recipient recip = null;
        Outlook.ExchangeUser exUser = null;
        string sAddress = "";
        try
        {
            var address = appointment.GetOrganizer();

            if (address.AddressEntryUserType == Outlook.OlAddressEntryUserType.olExchangeUserAddressEntry ||
                address.AddressEntryUserType == Outlook.OlAddressEntryUserType.olExchangeRemoteUserAddressEntry)
            {
                recip = Globals.ThisAddIn.Application.Session.CreateRecipient(address.Address);
                exUser = recip?.AddressEntry?.GetExchangeUser();
                sAddress = exUser?.PrimarySmtpAddress;
                if (string.IsNullOrEmpty(sAddress))
                {
                    sAddress = address?.Address?.Replace("'", "");
                }
            }
            else
            {
                sAddress = address?.Address?.Replace("'", "");
            }
        }
        catch (Exception ex)
        {
            Log.Error(ex, "GetSmtpAddress appointment Error");
        }
        finally
        {
            if (exUser != null) Marshal.ReleaseComObject(exUser);
            if (recip != null) Marshal.ReleaseComObject(recip);
        }
        return sAddress;
    }

    public static string GetSmtpAddress(this Outlook.Recipient recipient)
    {
        Outlook.ExchangeUser exUser = null;
        string sAddress = "";
        try
        {
            exUser = recipient.AddressEntry?.GetExchangeUser();
            if (exUser != null)
            {
                sAddress = exUser?.PrimarySmtpAddress;
                if (string.IsNullOrEmpty(sAddress))
                    sAddress = recipient.Address.Replace("'", "");
            }
            else
            {
                sAddress = recipient.Address.Replace("'", "");
            }
        }
        catch (Exception ex)
        {
            Log.Error(ex, "GetSmtpAddress Recipient Error");
        }
        finally
        {
            if (exUser != null) Marshal.ReleaseComObject(exUser);
        }




        return sAddress;
    }

    public static string GetSmtpAddress(this Outlook.AddressEntry recipient)
    {
        Outlook.ExchangeUser exUser = null;
        string sAddress = "";
        try
        {
            exUser = recipient?.GetExchangeUser();
            if (exUser != null)
            {
                sAddress = exUser?.PrimarySmtpAddress;
                if (string.IsNullOrEmpty(sAddress))
                    sAddress = recipient.Address.Replace("'", "");
            }
            else
            {
                sAddress = recipient?.Address.Replace("'", "");
            }
        }
        catch (Exception ex)
        {
            Log.Error(ex, "GetSmtpAddress Recipient Error");
        }
        finally
        {
            if (exUser != null) Marshal.ReleaseComObject(exUser);
        }

        return sAddress;
    }

    public static string GetSenderEmailAddress(this Outlook.MeetingItem mapiObject)
    {
        try
        {
            Outlook.PropertyAccessor oPA;
            string propName = "http://schemas.microsoft.com/mapi/proptag/0x0065001F";
            oPA = mapiObject.PropertyAccessor;
            string email = oPA.GetProperty(propName).ToString();
            return email;
        }
        catch (Exception ex)
        {
            Log.Error(ex, "GetSenderEmailAddress");
        }

        return "";
    }


    private static string fileLogFolder
    {
        get
        {
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Sustainably Run Meetings plugin\\log\\files\\";
            if (Directory.Exists(folder) == false)
            {
                Directory.CreateDirectory(folder);
            }
            return folder;
        }
    }


    public static void SaveToFile(this string text)
    {
        try
        {
            var path = fileLogFolder + $"{DateTime.Now.ToFileTime()}.txt";
            File.WriteAllText(path, text);
        }
        catch (Exception ex)
        {

        }
    }

    public static void ForEach<T>(this IEnumerable<T> ie, Action<T> action)
    {
        foreach (var i in ie)
        {
            action(i);
        }
    }

    public static bool IsValid(string emailaddress)
    {
        try
        {
            MailAddress m = new MailAddress(emailaddress);

            return true;
        }
        catch (FormatException)
        {
            return false;
        }
    }

    public static List<string> GetWeekDaysSelected(Outlook.OlDaysOfWeek dayOfWeekMask)
    {
        List<string> weekDaysSelected = new List<string>();

        if ((dayOfWeekMask & Outlook.OlDaysOfWeek.olMonday) != 0)
        {
            weekDaysSelected.Add("monday");
        }
        if ((dayOfWeekMask & Outlook.OlDaysOfWeek.olTuesday) != 0)
        {
            weekDaysSelected.Add("tuesday");
        }
        if ((dayOfWeekMask & Outlook.OlDaysOfWeek.olWednesday) != 0)
        {
            weekDaysSelected.Add("wednesday");
        }
        if ((dayOfWeekMask & Outlook.OlDaysOfWeek.olThursday) != 0)
        {
            weekDaysSelected.Add("thursday");
        }
        if ((dayOfWeekMask & Outlook.OlDaysOfWeek.olFriday) != 0)
        {
            weekDaysSelected.Add("friday");
        }
        if ((dayOfWeekMask & Outlook.OlDaysOfWeek.olSaturday) != 0)
        {
            weekDaysSelected.Add("saturday");
        }
        if ((dayOfWeekMask & Outlook.OlDaysOfWeek.olSunday) != 0)
        {
            weekDaysSelected.Add("sunday");
        }

        return weekDaysSelected;

    }
}
