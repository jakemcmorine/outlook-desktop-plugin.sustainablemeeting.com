﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using MeetingPlugin.Code.API.Controller;
using MeetingPlugin.Code.API.Entity;
using MeetingPlugin.Code.Entity;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace MeetingPlugin.Code.Controller
{
    public class CalendarController
    {
        public async Task<bool> Add(Outlook.MeetingItem meeting)
        {
            Outlook.AppointmentItem appointment = meeting.GetAssociatedAppointment(false);
            try
            {
                return await Add(appointment);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.Add");
            }
            finally
            {

            }
            return false;
        }

        public async Task<MeetingInfoEntity> GetMeetingInfo(Outlook.AppointmentItem appointment)
        {
            MeetingInfoEntity info = null;
            try
            {
                MeetingController controller = new MeetingController();
                info = await controller.GetMeeting(appointment.GlobalAppointmentID);
                return info;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.GetMeetingInfo");
            }
            finally
            {

            }
            return info;
        }
        public async Task<bool> Add(Outlook.AppointmentItem appointment, bool IsDesktop = false)
        {
            Outlook.UserProperties userProperties = appointment.UserProperties;
            try
            {
                //Globals.ThisAddIn.Monitor.CacheNewId.Add(appointment.EntryID);
                var entity = await appointment.ToEntity();
                //ThisAddIn.meetingObjectsList.Add(entity);
                //if (appointment.IsRecurring)
                //{
                //    var pattern = appointment.GetRecurrencePattern();
                //    if (pattern.NoEndDate == false)
                //    {
                //        var endDate = pattern.PatternEndDate;
                //        if (endDate < DateTime.Now)
                //        {
                //            Log.Info("Skip - old Meeting CalendarController.Add");
                //            return false;
                //        }
                //    }
                //}

                MeetingResEntity dbMeeting = null;

                if (appointment.IsRecurring)
                {
                    //RecurringController rController = new RecurringController();                   
                    //dbMeeting = await rController.Create(entity);
                }
                else
                {
                    MeetingController controller = new MeetingController();
                    dbMeeting = await controller.Create(entity);
                }



                if (dbMeeting != null)
                {
                    entity.SMID = dbMeeting.MeetingId;
                    ThisAddIn.meetingObjectsList.Add(entity);

                    if (userProperties["SustainableMeetingId"] == null)
                        userProperties.Add("SustainableMeetingId", Outlook.OlUserPropertyType.olText, true);

                    userProperties["SustainableMeetingId"].Value = dbMeeting.MeetingId;

                    //
                    bool IsIgnore = false;
                    if (userProperties["SustainableMeetingIgnore"] != null)
                    {
                        if ((bool)userProperties["SustainableMeetingIgnore"].Value)
                        {
                            IsIgnore = true;
                        }
                    }

                    if ((appointment.Categories + "").Contains(CategoryController.CategoriesName) == false && !IsIgnore)
                        appointment.Categories += CategoryController.CategoriesName;

                    if (IsDesktop && userProperties["SustainableDesktopMeeting"] == null)
                    {
                        userProperties.Add("SustainableDesktopMeeting", Outlook.OlUserPropertyType.olYesNo, true);
                        userProperties["SustainableDesktopMeeting"].Value = true;
                    }
                    //appointment.Subject = $"GEA#{dbMeeting.MeetingId}# : {appointment.Subject}";                    
                    appointment.Save();



                    return true;
                }
                else
                {
                    Log.Info("Error in creating meeting");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.Add");
            }
            finally
            {
                //Globals.ThisAddIn.Monitor.CacheNewId.Remove(appointment.EntryID);
                Marshal.ReleaseComObject(userProperties);
                userProperties = null;
            }
            return false;
        }

        public async Task<bool> Edit(Outlook.MeetingItem meeting)
        {
            Outlook.AppointmentItem appointment = meeting.GetAssociatedAppointment(false);
            try
            {
                if (appointment == null)
                {
                    var meetingId = "";
                    string subject = meeting.Subject + "";
                    int index = subject.IndexOf("GEA#") + 4;
                    int last = subject.IndexOf("#", index);
                    if (index != -1 && last != -1)
                    {
                        meetingId = subject.Substring(index, last - index);
                    }

                    //search in calendar
                    var folder = Globals.ThisAddIn.Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);
                    appointment = folder.Items.Cast<Outlook.AppointmentItem>().FirstOrDefault(c => (c?.Subject + "").Contains($"GEA#{meetingId}#"));
                }

                return await Edit(appointment);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.Edit");
            }
            finally
            {
                if (appointment != null) Marshal.ReleaseComObject(appointment);
            }
            return false;
        }

        public async Task<bool> Edit(Outlook.AppointmentItem appointment)
        {
            Outlook.UserProperties userProperties = appointment.UserProperties;
            try
            {
                string meetingId = null;

                if (appointment == null)
                    return false;

                if ((appointment.Subject + "").Contains("GEA#"))
                {
                    string subject = appointment.Subject;
                    int index = subject.IndexOf("GEA#") + 4;
                    int last = subject.IndexOf("#", index);
                    if (index != -1 && last != -1)
                    {
                        meetingId = subject.Substring(index, last - index);
                    }
                }

                if (string.IsNullOrEmpty(meetingId))
                {
                    //if (!(appointment.Categories + "").Contains(CategoryController.CategoriesName))
                    //    return false;
                    if (userProperties["SustainableMeetingId"] != null)
                    {
                        meetingId = userProperties["SustainableMeetingId"].Value + "";
                    }
                }

                int id = 0;

                if (appointment.IsRecurring)
                {
                    //To fix bug: When normal meeting converted to Recurring meeting, have to call Cancel Normal Meeting API and remove 'Sustainable category' 
                    if (string.IsNullOrEmpty(meetingId) == false)
                    {                        
                        await Cancel(meetingId, appointment.GlobalAppointmentID, false);

                        if ((appointment.Categories + "").Contains(CategoryController.CategoriesName) == true)
                        {
                            appointment.Categories = appointment.Categories.Replace(string.Format("{0}, ", CategoryController.CategoriesName), "").Replace(string.Format("{0}", CategoryController.CategoriesName), "");
                            appointment.Save();
                        }
                    }


                    //var entity = await appointment.ToEntity();
                    ////Outlook.Exceptions exceptions = appointment.GetRecurrencePattern().Exceptions;
                    //Outlook.RecurrencePattern recurrencePattern = appointment.GetRecurrencePattern();
                    //Outlook.Exceptions exceptions = recurrencePattern.Exceptions;
                    //Outlook.AppointmentItem appt = null;
                    //for (int i = 1; i <= exceptions.Count; i++)
                    //{
                    //    if (exceptions[i].Deleted)
                    //    {
                    //        MeetingEntity deletedMeetingInstance = new MeetingEntity();
                    //        deletedMeetingInstance.IsCancelled = true;
                    //        //deletedMeetingInstance.OriginalDate = exceptions[i].OriginalDate;
                    //        deletedMeetingInstance.OriginalDate = DateAndTimeAdd(exceptions[i].OriginalDate, appointment.Start).ToUniversalTime();
                    //        entity.SubAppointmentsException.Add(deletedMeetingInstance);

                    //    }
                    //    else
                    //    {

                    //        appt = exceptions[i].AppointmentItem;
                    //        var apptEntity = await appt.ToEntity();
                    //        //DateTime checkOriginal = exceptions[i].OriginalDate;
                    //        if (exceptions[i].OriginalDate != null)
                    //        {
                    //            apptEntity.OriginalDate = DateAndTimeAdd(exceptions[i].OriginalDate, appointment.Start).ToUniversalTime();
                    //        }
                    //        entity.SubAppointmentsException.Add(apptEntity);
                    //    }

                    //}

                    //RecurringController rController = new RecurringController();
                    //var updateId = await rController.Update(id, entity);

                    //Marshal.ReleaseComObject(recurrencePattern);
                    //recurrencePattern = null;
                    //Marshal.ReleaseComObject(exceptions);
                    //exceptions = null;
                    //if (appt != null)
                    //{
                    //    Marshal.ReleaseComObject(appt);
                    //    appt = null;
                    //}
                }
                else
                {
                    if (string.IsNullOrEmpty(meetingId) == false)
                    {
                        id = int.Parse("0" + meetingId);

                        var entity = await appointment.ToEntity();
                        MeetingEntity existingEntity = ThisAddIn.meetingObjectsList.Where(e => e.OutlookId == entity.OutlookId).FirstOrDefault();
                        if (CompareMeetingEntity(existingEntity, entity))
                        {
                            if (!(ThisAddIn.ResponseStatusList.Contains(entity.OutlookId)))
                            {
                                CompareMeetingInviteStatus(existingEntity, entity, id);
                            }

                            return true;
                        }
                        if (existingEntity == null)
                        {
                            entity.SMID = meetingId;
                            ThisAddIn.meetingObjectsList.Add(entity);
                        }
                        else
                        {
                            ThisAddIn.meetingObjectsList.Remove(existingEntity);
                            entity.SMID = meetingId;
                            ThisAddIn.meetingObjectsList.Add(entity);
                        }
                        MeetingController controller = new MeetingController();
                        var updateId = await controller.Update(id, entity);
                        if (updateId.HasValue)
                        {
                            await controller.UpdateInvites(updateId.Value, entity);
                            if (!(ThisAddIn.ResponseStatusList.Contains(entity.OutlookId)))
                            {
                                CompareMeetingInviteStatus(existingEntity, entity, id);
                            }
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.Edit");
            }
            finally
            {
                if (appointment != null)
                {
                    Marshal.ReleaseComObject(appointment);
                    appointment = null;
                }
                Marshal.ReleaseComObject(userProperties);
                userProperties = null;
            }
            return false;
        }

        public async Task<bool> Cancel(Outlook.MeetingItem meeting)
        {
            Outlook.AppointmentItem appointment = meeting.GetAssociatedAppointment(false);
            try
            {
                await Cancel(appointment);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.Cancel(meeting)");
            }
            finally
            {
                if (appointment != null) Marshal.ReleaseComObject(appointment);
            }
            return false;
        }

        public async Task<bool> Cancel(Outlook.AppointmentItem appointment)
        {
            try
            {
                string meetingId = null;

                if (appointment == null)
                    return false;

                if ((appointment.Subject + "").Contains("GEA#"))
                {
                    string subject = appointment.Subject;
                    int index = subject.IndexOf("GEA#") + 4;
                    int last = subject.IndexOf("#", index);
                    if (index != -1 && last != -1)
                    {
                        meetingId = subject.Substring(index, last - index);
                    }
                }

                if (string.IsNullOrEmpty(meetingId))
                {
                    //if (!(appointment.Categories + "").Contains(CategoryController.CategoriesName))
                    //    return false;
                    if (appointment.UserProperties["SustainableMeetingId"] != null)
                    {
                        meetingId = appointment.UserProperties["SustainableMeetingId"].Value + "";
                    }
                }


                if (string.IsNullOrEmpty(meetingId) == false)
                {
                    int id = int.Parse("0" + meetingId);

                    if (appointment.IsRecurring)
                    {
                        //RecurringController rController = new RecurringController();
                        //var result = await rController.Cancel(id, appointment.GlobalAppointmentID);
                        //return result;
                    }
                    else
                    {
                        MeetingController controller = new MeetingController();
                        var result = await controller.Cancel(id, appointment.EntryID);
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.Cancel(appointment)");
            }
            finally
            {
                if (appointment != null)
                {
                    Marshal.ReleaseComObject(appointment);
                    appointment = null;
                }
            }
            return false;
        }

        public async Task<bool> Cancel(string meetingId, string outlookId, bool IsRecurring = false)
        {
            try
            {
                if (string.IsNullOrEmpty(meetingId) == false)
                {
                    if (IsRecurring)
                    {
                        //RecurringController rcontroller = new RecurringController();
                        //int id = int.Parse("0" + meetingId);
                        //var result = await rcontroller.Cancel(id, outlookId);
                        //return result;
                    }
                    else
                    {
                        MeetingController controller = new MeetingController();
                        int id = int.Parse("0" + meetingId);
                        var result = await controller.Cancel(id, meetingId);
                        return result;
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.Cancel(appointment)");
            }
            finally
            {

            }
            return false;
        }
        public async Task<bool> Status(Outlook.MeetingItem meeting, string email, int exStatus = -1)
        {
            Outlook.AppointmentItem appointment = meeting.GetAssociatedAppointment(false);
            try
            {
                await Status(appointment, email, exStatus);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.Status(meeting)");
            }
            finally
            {
                if (appointment != null)
                {
                    Marshal.ReleaseComObject(appointment);
                    appointment = null;
                }
            }
            return false;
        }

        public async Task<bool> Status(Outlook.AppointmentItem appointment, string email, int exStatus = -1)
        {
            Outlook.UserProperties userProperties = appointment.UserProperties;
            try
            {
                string meetingId = null;
                string subId = null;

                if (appointment == null)
                {
                    Log.Info("Associated appointment is not Correct");
                    return false;
                }

                if ((appointment.Subject + "").Contains("GEA#"))
                {
                    string subject = appointment.Subject + "";
                    int index = subject.IndexOf("GEA#") + 4;
                    int last = subject.IndexOf("#", index);
                    if (index != -1 && last != -1)
                    {
                        meetingId = subject.Substring(index, last - index);
                    }
                }

                if (string.IsNullOrEmpty(meetingId))
                {
                    //if (!(appointment.Categories + "").Contains(CategoryController.CategoriesName))
                    //    return false;
                    if (userProperties["SustainableMeetingId"] != null)
                    {
                        meetingId = userProperties["SustainableMeetingId"].Value + "";
                    }

                    if (userProperties["SustainableSubMeetingId"] != null)
                    {
                        subId = userProperties["SustainableSubMeetingId"].Value + "";
                    }
                }

                //return true; ;
                appointment.Recipients.ResolveAll();

                int status = exStatus;
                if (exStatus == -1)
                {
                    var rec = appointment.Recipients.Cast<Outlook.Recipient>().FirstOrDefault(c => c.Address == email);

                    if (rec.MeetingResponseStatus == Outlook.OlResponseStatus.olResponseAccepted)
                    {
                        status = 1;
                    }
                    else if (rec.MeetingResponseStatus == Outlook.OlResponseStatus.olResponseDeclined)
                    {
                        status = 2;
                    }
                    else if (rec.MeetingResponseStatus == Outlook.OlResponseStatus.olResponseTentative)
                    {
                        status = 3;
                    }
                    else
                    {
                        status = 4;
                    }
                    if (rec != null)
                    {
                        Marshal.ReleaseComObject(rec);
                        rec = null;
                    }
                }



                if (string.IsNullOrEmpty(subId) == false)
                {
                    int id = int.Parse("0" + subId);
                    SubMeetingController controller = new SubMeetingController();
                    var result = await controller.Status(id, status, email, appointment.EntryID);

                    return result;
                }
                else if (string.IsNullOrEmpty(meetingId) == false)
                {
                    int id = int.Parse("0" + meetingId);
                    MeetingController controller = new MeetingController();
                    var result = await controller.Status(id, status, email, appointment.EntryID);

                    return result;
                }
                else
                {
                    Log.Info("meetingId Missed No Update happen");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.Status(appointment)");
            }
            finally
            {
                if (userProperties != null)
                {
                    Marshal.ReleaseComObject(userProperties);
                    userProperties = null;
                }
                if (appointment != null)
                {
                    Marshal.ReleaseComObject(appointment);
                    appointment = null;
                }
            }
            return false;
        }

        public async Task<bool> Status(int meetingId, string email, Outlook.OlResponseStatus meetingResponse)
        {

            try
            {

                if (meetingId == null)
                {
                    Log.Info("Associated appointment is not Correct");
                    return false;
                }



                int status = 4;


                if (meetingResponse == Outlook.OlResponseStatus.olResponseAccepted)
                {
                    status = 1;
                }
                else if (meetingResponse == Outlook.OlResponseStatus.olResponseDeclined)
                {
                    status = 2;
                }
                else if (meetingResponse == Outlook.OlResponseStatus.olResponseTentative)
                {
                    status = 3;
                }
                else
                {
                    status = 4;
                }
                MeetingController controller = new MeetingController();
                int id = int.Parse("0" + meetingId);
                var result = await controller.Status(id, status, email, meetingId.ToString());

                return result;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "CalendarController.Status(appointment)");
            }
            finally
            {

            }
            return false;
        }

        private bool CompareMeetingEntity(MeetingEntity firstObject, MeetingEntity secondObject)
        {
            if (firstObject == null || secondObject == null)
            {
                return false;
            }

            return firstObject.Location == secondObject.Location &&
                firstObject.Start == secondObject.Start &&
                firstObject.End == secondObject.End &&
                firstObject.Invites.Length == secondObject.Invites.Length &&
                CheckAttendeeList(firstObject, secondObject);
        }
        private bool CompareMeetingInviteStatus(MeetingEntity firstObject, MeetingEntity secondObject, int id)
        {
            if (firstObject == null || secondObject == null)
            {
                return false;
            }

            for (int i = 0; i < firstObject.Invites.Length; i++)
            {
                string currentEmail = firstObject.Invites[i].Email;
                InviteEntity newInviteEntity = secondObject.Invites.Where(invitee => invitee.Email == currentEmail).FirstOrDefault();
                if (newInviteEntity != null)
                {
                    if (newInviteEntity.meetingResponse != Outlook.OlResponseStatus.olResponseNone &&
                        newInviteEntity.meetingResponse != Outlook.OlResponseStatus.olResponseNotResponded &&
                        firstObject.Invites[i].meetingResponse != newInviteEntity.meetingResponse)
                    {
                        firstObject.Invites[i].meetingResponse = newInviteEntity.meetingResponse;
                        Status(id, currentEmail, newInviteEntity.meetingResponse);
                    }
                }
            }
            return true;
        }

        private bool CheckAttendeeList(MeetingEntity firstObject, MeetingEntity secondObject)
        {
            bool ret = true;
            List<InviteEntity> firstInviteList = firstObject.Invites.ToList();
            for (int i = 0; i < secondObject.Invites.Length; i++)
            {
                if (firstInviteList.Where(invitee => invitee.Email == secondObject.Invites[i].Email).Count() > 0)
                    continue;
                else
                {
                    ret = false;
                    break;
                }

            }
            return ret;
        }

        public static DateTime DateAndTimeAdd(DateTime takeOnlyDate, DateTime takeOnlyTime)
        {
            return new DateTime(takeOnlyDate.Year, takeOnlyDate.Month, takeOnlyDate.Day, takeOnlyTime.Hour, takeOnlyTime.Minute, takeOnlyTime.Second);
        }
    }
}
