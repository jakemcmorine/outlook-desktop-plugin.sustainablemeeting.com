﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.Entity
{
    public class AppEntity
    {
        public OperationEntity Operation { get; set; }
        public string MeetingId { get; set; }
        public string Email { get; set; }
        public bool IsDone { get; set; }

        public override bool Equals(object obj)
        {
            var entity = obj as AppEntity;
            return MeetingId ==entity?.MeetingId;
        }
    }

    public enum OperationEntity
    {
        Add,Edit,Cancel,Status
    }
}
