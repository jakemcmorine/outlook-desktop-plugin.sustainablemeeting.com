﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetingPlugin.Code.Entity
{
    public class RequestEntity
    {
        public string Token { get; set; }
        public string subject { get; set; }
        public string location { get; set; }
        public string type { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string OutlookID { get; set; }
        public string AllDay { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string invites { get; set; }
    }
}
