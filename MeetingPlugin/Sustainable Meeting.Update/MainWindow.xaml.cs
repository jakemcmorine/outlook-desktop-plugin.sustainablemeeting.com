﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace Sustainable_Meeting.Update
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string version { get; set; }
        string TempPath { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }
        

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            TxtCurrent.Content = $"{App.LocalVersion}";
            TxtNew.Content = $"{App.NewVersion}";

            version = App.NewVersion+"";

            TempPath= $"{ System.IO.Path.GetTempPath()}{DateTime.Now.ToFileTime()}.msi"; 
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ShowLoading();

                //string url = $"https://sustainable-meeting.com/download/Setup.{version}.msi";
                string url = $"http://localhost:8080/app/download/Setup.{version}.msi";
                Thread thread = new Thread(() =>
                {
                    WebClient client = new WebClient();
                    //client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                    client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
                    client.DownloadFileAsync(new Uri(url), TempPath);
                });
                thread.Start();

            }
            catch (Exception ex)
            {

                HideLoading();
            }
            finally
            {

            }
        }

        void ShowLoading()
        {
            Dispatcher.Invoke(() => { PnlLoading.Visibility = Visibility.Visible; });
        }

        void HideLoading()
        {
            Dispatcher.Invoke(() => { PnlLoading.Visibility = Visibility.Collapsed; });
        }

        void ShowMessage()
        {
            Dispatcher.Invoke(() => { PnlMessage.Visibility = Visibility.Visible; });
        }
        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                if (File.Exists(TempPath))
                {
                    Process process = new Process();
                    process.StartInfo.FileName = "msiexec";
                    process.StartInfo.WorkingDirectory = System.IO.Path.GetTempPath();
                    process.StartInfo.Arguments = $" /quiet /i {Path.GetFileName(TempPath)}";
                    process.StartInfo.Verb = "runas";
                    process.Start();

                    while (process.HasExited == false)
                    {

                    }
                    HideLoading();
                    ShowMessage();
                }
            }
            catch (Exception ex)
            {
                HideLoading();
            }
        }

        private void BtnRestart_OnClick(object sender, RoutedEventArgs e)
        {
            var processes = Process.GetProcessesByName("OUTLOOK");
            if (processes?.Count() > 0)
            {
                foreach (Process process in processes)
                {
                    process.Kill();
                }
            }

            Process.Start("OUTLOOK");
        }

        private void BtnLater_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
