﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Sustainable_Meeting.Update
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Version LocalVersion { get; set; }
        public static Version NewVersion { get; set; }
        protected override void OnStartup(StartupEventArgs e)
        {
            if (e.Args.Length == 2)
            {
                string v1 = e.Args[0];
                string v2 = e.Args[1];

                Version.TryParse(v1, out var dbVersion);
                LocalVersion = dbVersion;

                Version.TryParse(v2, out var currentVersion);
                NewVersion = currentVersion;
            }

            base.OnStartup(e);
        }
    }
}
